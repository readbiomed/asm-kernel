

# Approximate Subgraph Matching (ASM) Kernel 
 This repository contains our implementation of the graph kernels for high performance biomedical relation extraction, as described in our [paper](https://jbiomedsem.biomedcentral.com/articles/10.1186/s13326-017-0168-3) :

The relevant papers for these works that you can cite are :

*  Exploiting graph kernels for high performance biomedical relation extraction. Nagesh C. Panyam , Karin Verspoor , Trevor Cohn and Kotagiri Ramamohanarao. Journal of Biomedical Semantics. 2018 .

    * In this paper, we present a novel graph kernel, namely the Approximate Subgraph Matching Kernel (ASM), followed with a detailed comparative study of ASM with the popular All Path Graph (APG) kernel for biomedical relation extraction. 


* Approximate Subgraph Matching (ASM) : Deriving a Mercer Kernel from the classic ASM distance measure

    *   Please see this [paper](http://alta2016.alta.asn.au/U16/U16-1007.pdf) for more details on the derivation of ASM kernel from the classical ASM distance measure. 
        *  ASM Kernel: Graph Kernel using Approximate Subgraph Matching for Relation Extraction. Nagesh C. Panyam,  Karin Verspoor, Trevor Cohn  and Kotagiri Ramamohanarao. Australasian Language Technology Association Workshop 2016.
 
* All Path Graph Kernel (APG) . 

    *   Please see this [paper](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-9-S11-S2) for more details. 
        * All-paths graph kernel for protein-protein interaction extraction with evaluation of cross-corpus learning. Antti Airola, Sampo Pyysalo, Jari Björne, Tapio Pahikkala, Filip Ginter and Tapio Salakoski, BMC Bioinformatics, 2008.

   
* The details of the Chemical Induced Disease (CID) Relation extraction task and the corresponding corpus can be obtained from  [BioCreative-V, 2015](http://www.biocreative.org/resources/corpora/biocreative-v-cdr-corpus/). 
* The details of the Protein-Protein-Interaction (PPI) extraction task and the PPI corpora is available [here](http://mars.cs.utu.fi/PPICorpora/)

* The original Aproximate Subgraph Matching  distance is described (code and paper) at the following [site](http://asmalgorithm.sourceforge.net/). Please refer this, for finding the relevant sources to cite.


#License
The terms of the MIT License,
The MIT License (MIT) Copyright (c) 2013 Thomas Park
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
THE USE OR OTHER DEALINGS IN THE SOFTWARE.


# Instructions for running: ##
* Prerequisites : 
    * Java and Python. 
    *  The classical Approximate Subgraph Matching [ASM](http://asmalgorithm.sourceforge.net/) distance measure. The pom.xml contains a reference to this maven repository.
    *  Stanford [CoreNLP](https://stanfordnlp.github.io/CoreNLP/), including the model files are required for parsing the text files.  Ensure the environment variable CLASSPATH is updated with the model file's location. 
    * [Kelp](https://github.com/SAG-KeLP/kelp-full) is needed for running the SVM classifiers. 
    * For classification with large datasets, such as CID non-sentence level relations, we recommend a machine with atleast 15GB RAM.  Many of the experiments such as parsing and classification over different datasets can be run in parallel. In our work, we used a server with 34GB RAM and modified these scripts to run 2 or 3 classification tasks in background (parallel) mode. 

* CID relation extraction:
    * Create a directory "./cdr_data/source_files_from_cdr/" and place the CID source files here:  <br>
     (CDR_DevelopmentSet.PubTator.txt, CDR_TestSet.PubTator.txt and CDR_TrainingSet.PubTator.PubTator.txt ) 
    * run "bash cdr_experiments.sh"
        * Step 1 is parsing, that takes about 30 minutes. Step 2 is the main classification task. Graph kernels, APG and ASM takes about 9+ hours. Final step of performance measurement should print the results presented in Table 2 of our ASM paper. 


* PPI extraction :
    * Create a directory "./ppi_data/" and place the PPI corpora here : <br>
      (  AImed-learning-format.xml, BioInfer-learning-format.xml, HPRD50-learning-format.xml, IEPA-learning-format.xml and LLL-learning-format.xml)
    * run "bash ppi_experiments.sh"
       * Parsing the 5 PPI corpora and producing ASM flat features takes about 1.5 hours. Classification of PPI corpora can take 7+ hrs for each dataset.  

## Contact  ###
* npanyam AT student DOT unimelb DOT edu DOT au


