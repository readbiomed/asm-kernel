package myutils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.OpenMapRealMatrix;

//import org.apache.commons.math.linear.Array2DRowRealMatrix;
//import org.apache.commons.math.linear.ArrayRealVector;
//import org.apache.commons.math.linear.DecompositionSolver;
//import org.apache.commons.math.linear.LUDecompositionImpl;
//import org.apache.commons.math.linear.RealMatrix;
//import org.apache.commons.math.linear.RealVector;

import org.apache.commons.math3.linear.RealMatrix;

import edu.ucdenver.ccp.asm.ASM;
import edu.ucdenver.ccp.asm.Edge;
import edu.ucdenver.ccp.asm.Vertex;
import edu.uci.ics.jung.graph.DirectedGraph;
import it.uniroma2.sag.kelp.data.representation.string.StringRepresentation;
import it.uniroma2.sag.kelp.kernel.DirectKernel;;

@SuppressWarnings("deprecation")

public class APGKernel extends DirectKernel<StringRepresentation>{
	
	private String asmGraphRepresentationName;
	private String wordFirstEntity;
	private String wordSecondEntity;
	private HashMap<Integer, DirectedGraph<Vertex,Edge> >  asmGraphCache = new HashMap<Integer, DirectedGraph<Vertex,Edge> >();
	private HashMap<Integer, HashMap<Integer, HashMap<Integer,Float>> >  apgGMatrixCache = new HashMap<Integer, HashMap<Integer, HashMap<Integer,Float> > >();
	
	//The edge weight assignments, as prescribed in the APG paper.
	private final float DEFAULT_EDGE_WEIGHT = 0.3f;
	private final float SDP_EDGE_WEIGHT = 0.9f;
	
	public APGKernel(String kelp_field_name, String startWord, String endWord) {
		super(kelp_field_name);//have to inform the parent/base class to send me the field (which is |BS:asmGraph|..|ES|)
		this.asmGraphRepresentationName = kelp_field_name;
		this.wordFirstEntity =  startWord;
		this.wordSecondEntity   =  endWord;
	}


	private DirectedGraph<Vertex, Edge> get_asm_graph( String graphstr){
		DirectedGraph<Vertex, Edge> asmgraph = null;
		if ( false ==  asmGraphCache.containsKey(graphstr.hashCode()) ){
			//construct the regular asm style dependency graph with edge labels first 
			asmgraph = APSPGraph.createASMGraph(graphstr);
			asmGraphCache.put( graphstr.hashCode(), asmgraph);
		}else{
			asmgraph =  asmGraphCache.get( graphstr.hashCode() ); 
		}
		return asmgraph;
	}
	
	public static  List<Vertex> get_entity_shortestpath(DirectedGraph<Vertex,Edge> asmGraph , String wordFirstEntity, String wordSecondEntity){
		//process the asm vertices first 
		Vertex startNode= null;
		Vertex endNode = null;//also identify the entity nodes
		for ( Vertex v: asmGraph.getVertices()){
			//identify the entity nodes			
			if (v.getWord().contentEquals(wordFirstEntity)){ 
				if (startNode == null)  {
					startNode = v;
				} else if (startNode.getPosition() > v.getPosition()) { //in case of multiple start tokens, choose the earliest one
					startNode = v;
				}
			}
			
			if (v.getWord().contentEquals(wordSecondEntity)){ 
				if (endNode == null)  {
					endNode = v;
				} else if (endNode.getPosition() > v.getPosition()) {
					endNode = v;
				}
			}
		}
				
		List<Vertex> shortestPathVertices = null;
		if (null== startNode  || null==endNode){
			shortestPathVertices = new ArrayList<Vertex>(); //leave an empty shortest path for this graph.
			System.err.println("in APGKernel, no shortest path for this graph " + asmGraph.toString());
		}else{
			try {	
				//find the shortest path between entity nodes
				shortestPathVertices = ASM.findShortestPathByDFS(asmGraph, startNode, endNode);
			}catch ( Exception e){
				e.printStackTrace();
				System.err.println("in APGKernel. get shortest path failed for " + asmGraph.toString());
				System.exit(-1);	
			}
		}
		return shortestPathVertices;
	}
	
	public HashMap<Integer, HashMap<Integer, Float> > get_apg_gmatrix( String graph_str ){
		if ( apgGMatrixCache.containsKey(graph_str.hashCode()) ){
			return apgGMatrixCache.get( graph_str.hashCode());
		} //else compute
		
		
		DirectedGraph<Vertex, Edge> asmgraph = get_asm_graph(graph_str);
		List<Vertex> shortestPathVertices = get_entity_shortestpath(asmgraph, wordFirstEntity, wordSecondEntity) ;
		//find the region containing the first and second entities in the linear order graph.
		Integer entityRegionStart = asmgraph.getVertexCount(); 
		Integer entityRegionEnd = 0; 
		for( Vertex v : shortestPathVertices){
			if ( false == v.getWord().startsWith("Entity")){
				Integer vPos= v.getPosition();
				if (vPos < entityRegionStart){
					entityRegionStart = vPos;
				}
				if (vPos > entityRegionEnd){
					entityRegionEnd = vPos;
				}
			}
		}
		
		//construct the W= inverse of (I-A) -I matrix
		
		int apgNumberOfVertices = 2* asmgraph.getVertexCount() + asmgraph.getEdgeCount(); //nov every token leads to two nodes and every dep. edge becomes a node as well
		OpenMapRealMatrix apg_matrix = new OpenMapRealMatrix(apgNumberOfVertices, apgNumberOfVertices);
		
		//int nol = _vocabulary_.size() ;// no of labels
		//float[][] apg_matrix = new float[apg_nov][apg_nov]; //adjacency matrix
		//float[][]  label_allocation_matrix = new float [nol][apg_nov];
		//OpenMapRealMatrix label_allocation_matrix = new OpenMapRealMatrix(nol, apg_nov);
		
		// get a mapping from vertex/edge of ASM to integer ids of apg nodes
		HashMap<Vertex, Integer> mapASMNodeToId = new HashMap<Vertex, Integer>();
		HashMap<Edge, Integer>   mapIDtoASMNode = new HashMap<Edge, Integer>();
		
		//instead of label allocation matrix, store a mapping of labels to the apg node, to which it applies, in a HashMap 
		HashMap<Integer, ArrayList<Integer> > mapNodeIdToLabels = new HashMap<Integer, ArrayList<Integer> >();
		
		//process the asm graph's  vertices 
		for ( Vertex v: asmgraph.getVertices()){
			//assign it a  node id in the apg graph 
			int apg_nodeid = mapASMNodeToId.size();
			mapASMNodeToId.put(v, apg_nodeid); 
			
			//the labels for this node are lemma and pos tag (appended with "IP" . ie in path if this vertex is in shortest path)
			String suffix = "";
			if ( -1 != shortestPathVertices.indexOf(v) ){
				suffix= " IP";
			}
			Integer nodelemma =  (v.getLemma() + suffix).hashCode();
			Integer nodepostag = (v.getTag()   + suffix).hashCode();
			
			//update the label allocation matrix
			
			ArrayList<Integer> labels = new ArrayList<Integer>( Arrays.asList(nodelemma, nodepostag));
			mapNodeIdToLabels.put( apg_nodeid,  labels);
		}
		
		
		
		for ( Edge e: asmgraph.getEdges()){
			//assign a apg node id to it - every asm edge becomes an apg node 
			int apg_node_id= asmgraph.getVertexCount() + mapIDtoASMNode.size(); //the first part is the offset ..coz to n-1 are already assigned to asm vertices
			mapIDtoASMNode.put( e, apg_node_id);
			
			Integer edgelabel = e.getLabel().hashCode();
			ArrayList<Integer> labels = new ArrayList<Integer>( Arrays.asList(edgelabel));
		
			//update the label allocation matrix
			
			mapNodeIdToLabels.put( apg_node_id, labels);
			
			//this edge leads to edges in apg - update adjacency matrix. 
			//First see if it is a shortest path edge or not
			float edge_weight = DEFAULT_EDGE_WEIGHT; // default apg edge weight
		
			Vertex src = e.getGovernor();
			Vertex dst = e.getDependent();
			
			int srcidx = shortestPathVertices.indexOf(src);
			if ( srcidx != -1) {
				int dstidx = shortestPathVertices.indexOf(dst);
				if  (dstidx!=-1 && Math.abs(dstidx -srcidx)== 1) { //this edge is a shortest path edge
					edge_weight = SDP_EDGE_WEIGHT; 
				}
			}
			
			//this asm edge becomes two edges in the apg graph
			int src_node_id =  mapASMNodeToId.get(src);
			int dst_node_id =  mapASMNodeToId.get(dst);
			
			apg_matrix.setEntry(src_node_id, apg_node_id, edge_weight);//apg_matrix[src_node_id][apg_node_id]  = edge_weight;
			apg_matrix.setEntry(apg_node_id, dst_node_id, edge_weight);//apg_matrix[apg_node_id][dst_node_id]  = edge_weight;			
		}
		
		//The linear  subgaph: 
		// All tokens 0 to n becomes separate set of vertices with edges connected in linear order - get them sorted in linear order first
		ArrayList<Vertex> linearNodeList = new ArrayList<Vertex>(asmgraph.getVertices());
		Collections.sort(linearNodeList, new Comparator<Vertex>(){
		     public int compare(Vertex o1, Vertex o2){
		         if(o1.getPosition() == o2.getPosition())
		             return 0;
		         return o1.getPosition() < o2.getPosition() ? -1 : 1;
		     }
		});	
		
		
		//Edges for the linear subgraph
		int apg_nodeid_start = asmgraph.getVertexCount() + asmgraph.getEdgeCount() ; //offset for apg node id..|V| + |E| already taken up above
		int apg_nodeid_end   =  apg_nodeid_start + asmgraph.getVertexCount() -1; 
		float linear_edge_weight = 0.9f;
		
		for ( int vertexIdx = apg_nodeid_start; vertexIdx <=  apg_nodeid_end-1 ; vertexIdx ++){
			apg_matrix.setEntry(vertexIdx,vertexIdx+1, linear_edge_weight);
		}
		//Labels for the linear subgraph
		for (int vertexIdx = 0 ; vertexIdx < linearNodeList.size(); vertexIdx++ ){
			Vertex ver = linearNodeList.get(vertexIdx);
			String regionPrefix = ""; //mark the region w.r.t entities, that this vertex falls into
			if ( ver.getPosition() <= entityRegionStart){
				regionPrefix = "Before";
			}else if ( ver.getPosition() >= entityRegionEnd){
				regionPrefix = "After";
			}
			
			Integer lemma  =  (regionPrefix + ver.getLemma()).hashCode();
			Integer postag =  (regionPrefix + ver.getTag()).hashCode();
			ArrayList<Integer> labels = new ArrayList<Integer>(Arrays.asList(lemma, postag));
			int apg_nodeid=  apg_nodeid_start + vertexIdx ;
			mapNodeIdToLabels.put(apg_nodeid, labels);
			
		}
		//the linear subgraph is done 
		
		
		//From the label allocation matrix and adjacency matrix,  get the W matrix. See APG paper for W's definition
		RealMatrix identity = MatrixUtils.createRealIdentityMatrix(apgNumberOfVertices );
		RealMatrix i_a = identity.subtract(apg_matrix)  ;//  I - A
		RealMatrix i_a_inv = new LUDecomposition(i_a).getSolver().getInverse(); // (I-A)^-1
		RealMatrix wmat =  i_a_inv.subtract(identity) ; // W= (I-A)^-1   -I 
		
		//wmat[i,j] is the strength or weight of paths leading from node i to node j
		//In other words, from label of node i to label of node j.
		HashMap<Integer, HashMap<Integer, Float> > labelTolabelmap = new HashMap<Integer, HashMap<Integer,Float> >();
		for (int  i = 0 ; i< apgNumberOfVertices ; i++ ){
			for ( int j =0; j< apgNumberOfVertices; j++){
				float strength = (float)wmat.getEntry(i, j);

				ArrayList<Integer> labels_lst1 =  mapNodeIdToLabels.get(i);
				ArrayList<Integer> labels_lst2 =  mapNodeIdToLabels.get(j);

				for ( Integer label1 : labels_lst1){
					for ( Integer label2: labels_lst2){

						if (false == labelTolabelmap.containsKey(label1)){
							labelTolabelmap.put(label1, new HashMap<Integer,Float>() );
						}

						float previous_entry = 0;
						if ( labelTolabelmap.get(label1).containsKey(label2) ){
							previous_entry = labelTolabelmap.get(label1).get(label2);
						}
						
						if ( strength > previous_entry ){ //max mode, not sum mode as per the APG original code
							labelTolabelmap.get(label1).put( label2,  strength);
						}
					}
				}
			}
		}
		
		apgGMatrixCache.put( graph_str.hashCode() , labelTolabelmap);
		return labelTolabelmap;
		//return gmat;
	}
	
	
	@Override
	public float kernelComputation(StringRepresentation repA, StringRepresentation repB)  {
		float finalKernelValue = 0.0f;
		
		HashMap<Integer, HashMap<Integer, Float> >   G1 = get_apg_gmatrix(repA.getTextFromData());
		HashMap<Integer, HashMap<Integer, Float> > G2 = get_apg_gmatrix(repB.getTextFromData());
		
		
		for ( Integer srcLabel : G1.keySet()){
			if ( G2.containsKey(srcLabel)){
				for ( Integer dstLabel : G1.get(srcLabel).keySet() ){
					if (G2.get(srcLabel).containsKey(dstLabel)){
						float g1val = G1.get(srcLabel).get(dstLabel);
						float g2val = G2.get(srcLabel).get(dstLabel);
						finalKernelValue +=   ( g1val *g2val );
					}
				}
			}
		}
		
		return finalKernelValue;
	}


	public static void main(String[] args) throws Exception {
		String dataRepresentationName= "asmGraph"; //_with_entities";//"asmGraph";
		String startNodeWord= "Entity1"; String endNodeWord= "Entity2";
		String graphstr= "entity1_edge(Entity1-23/NN, IFN_s_alpha-3/NN); entity2_edge(IL_s__d_-12/NN, Entity2-24/NN); nn(measurements-1/NNS, Cytokines-0/NNS); nsubj(showed-5/VBD, measurements-1/NNS); nn(treatment-4/NN, IFN_s_alpha-3/NN); prep_during(measurements-1/NNS, treatment-4/NN); det(trend-7/NN, a-6/DT); dobj(showed-5/VBD, trend-7/NN); amod(levels-10/NNS, decreasing-9/VBG); prep_to(showed-5/VBD, levels-10/NNS); prep_of(levels-10/NNS, IL_s__d_-12/NN); num(_d__d_-16/CD, _d_-14/CD); prep_at(showed-5/VBD, _d__d_-16/CD); num(weeks-20/NNS, _d__d_-19/CD); conj_and(_d__d_-16/CD, weeks-20/NNS); prep_at(showed-5/VBD, weeks-20/NNS); self(_s_-21/PUNCT, _s_-21/PUNCT); self(to-8/TO, to-8/TO); self(during-2/IN, during-2/IN); self(_s_-15/PUNCT, _s_-15/PUNCT); self(at-13/IN, at-13/IN); self(of-11/IN, of-11/IN); self(and-18/CC, and-18/CC); self(_s_-17/PUNCT, _s_-17/PUNCT);";
		APGKernel apg = new APGKernel(dataRepresentationName, startNodeWord, endNodeWord );
		
		System.err.println("Computing gmat ");
		apg.get_apg_gmatrix(graphstr);
		System.err.println("done");	
	}
}
