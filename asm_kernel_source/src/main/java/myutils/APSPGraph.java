package myutils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activity.InvalidActivityException;

import edu.ucdenver.ccp.asm.Edge;
import edu.ucdenver.ccp.asm.Vertex;
import edu.uci.ics.jung.graph.DirectedGraph;
import it.uniroma2.sag.kelp.data.dataset.SimpleDataset;
import it.uniroma2.sag.kelp.data.example.Example;
import it.uniroma2.sag.kelp.data.representation.string.StringRepresentation;
import it.uniroma2.sag.kelp.kernel.DirectKernel;

import edu.ucdenver.ccp.asm.Vertex;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.List;

import it.uniroma2.sag.kelp.data.dataset.SimpleDataset;
import it.uniroma2.sag.kelp.data.example.Example;
import it.uniroma2.sag.kelp.data.label.Label;
import it.uniroma2.sag.kelp.kernel.Kernel;
import it.uniroma2.sag.kelp.kernel.cache.FixIndexKernelCache;
import it.uniroma2.sag.kelp.kernel.cache.FixIndexSquaredNormCache;
import it.uniroma2.sag.kelp.kernel.standard.LinearKernelCombination;
import it.uniroma2.sag.kelp.kernel.standard.NormalizationKernel;
import it.uniroma2.sag.kelp.kernel.tree.PartialTreeKernel;
import it.uniroma2.sag.kelp.kernel.tree.SubSetTreeKernel;
import it.uniroma2.sag.kelp.kernel.tree.SubTreeKernel;
import it.uniroma2.sag.kelp.kernel.vector.LinearKernel;
import it.uniroma2.sag.kelp.learningalgorithm.classification.libsvm.BinaryCSvmClassification;
import it.uniroma2.sag.kelp.learningalgorithm.classification.multiclassification.OneVsAllLearning;
import it.uniroma2.sag.kelp.predictionfunction.classifier.ClassificationOutput;
import it.uniroma2.sag.kelp.predictionfunction.classifier.Classifier;
import it.uniroma2.sag.kelp.utils.evaluation.MulticlassClassificationEvaluator;


//APSP = all pair shortest path - a wraper around the Floyd Warshall algorithm for all pair shortest path computation
public class APSPGraph {
	
	//edge directions as edge labels - names for that
	public static final String FORWARD_EDGE= "_FD_";
	public static final String BACKWARD_EDGE= "_BD_";
	public static final String EDGE_DISTANCE= "_ED_"; 
	private static final float DEFAULT_EDGE_WEIGHT =  0.3f;  //(float) Math.log(0.3);
	private static final float SDP_EDGE_WEIGHT 	=   0.9f;    // (float) Math.log(0.9);
	
	 
	
	private String wordForEntity1, wordForEntity2 ;
	
	private int [][] distanceMatrix =null; 
	private int [][] predecessorNode =null; //
	private Edge [][] edgeMatrix = null;
	private float [][] edgeWeights = null; // edge weights that correspond to feature weights, not graph edge weights per se.
	private HashMap<Vertex, Integer> mapNodeToId = null;
	private HashMap<Integer, Vertex>  mapIdToNode = null;
	private HashMap<Integer, HashMap<Integer, HashMap<String,Float> > >  pathFeatureMatrix = null; //maps of maps (instead of 2d array) of feature maps for a path between i to j 
	
	public APSPGraph(String startNodeWord, String endNodeWord) {
		this.wordForEntity1 = startNodeWord;
		this.wordForEntity2 = endNodeWord;	
	}

	public static void main(String[] args) throws Exception {
        //creates linearized feature representation of the given asm graph.
        if (args.length < 3) {
			System.err.println("Arguments required <Examples File>  <Asm Graph Field Name> <Output Examples file with asm flat features>" );
		}else{
			String examplesFile = args[0];
            String fieldName    = args[1];
            String outputFile   = args[2];
		
            PrintWriter fhExamples = new PrintWriter(new FileWriter(outputFile));

			SimpleDataset dataset = new SimpleDataset();
		    dataset.populate(examplesFile );

            int progress =0;
            for ( Example ex : dataset.getExamples() ){
                fhExamples.print( ex.toString() );

                //linearized  asm flat features
		    	String asm_graph_format = ex.getRepresentation(fieldName).getTextFromData();
  		  	   	DirectedGraph<Vertex,Edge> graph = createASMGraphWithLinearSubgraph( asm_graph_format);
  		  	   	Boolean use_undirected_paths = true;
  		  	   	ArrayList<Path> lst_paths=  (new APSPGraph("Entity1", "Entity2")).get_all_pair_shortest_path(graph, use_undirected_paths);
  		  	   	fhExamples.print("|BV:asmflatfeatures|"); 
  		  	   	HashMap<String, Float> asm_flat_features = Path.get_asm_flatfeatures_from_allpaths(lst_paths);
  		  	   	for ( String fname : asm_flat_features.keySet() ){
  		  	   		fhExamples.print( fname.replace(":", "_") + ":" +  asm_flat_features.get(fname) + " ");
  		  	   	}
  		  	   	fhExamples.print("|EV|");
                
                fhExamples.println("");//end of line for end of example
                
                if ( (progress++ % 1000)  == 0){
                    System.err.println("No of examples processed =" +  progress / 1000  +  " K");
                }
            } 
            fhExamples.close();	

            System.err.println("Done");
		}				
	}
	
	public ArrayList<Path> get_all_pair_shortest_path(DirectedGraph<Vertex,Edge> graph, boolean undirected_paths) throws InvalidActivityException {

		//Floyd Warshall algorithm
		//Step 1 : initialize/setup matrices and auxiliary datastructures
		
		
		List<Vertex> sdp_vertices = APGKernel.get_entity_shortestpath(graph, wordForEntity1, wordForEntity2);
		
		//These two maps to navigate between node/vertex and integer ids -- used in floyd warshall's algo
		mapNodeToId =  new HashMap<Vertex,Integer>();
		mapIdToNode = new HashMap<Integer, Vertex>();
		
		for (Vertex v : graph.getVertices()) {
				mapNodeToId.put(v , mapNodeToId.size() );
				mapIdToNode.put(  mapNodeToId.get(v), v );
			
		}
			
		int numVertices = mapNodeToId.size();
		edgeWeights  = new float [numVertices][numVertices];
		for ( int i=0; i<numVertices ; i++){
			for (int j=0; j<numVertices; j++){
				edgeWeights[i][j] = DEFAULT_EDGE_WEIGHT; //will be overwritten with shortest dependency path weights later
			}
		}
					
		
		//Step 2: Initialize floyd warshall matrices and helper datastructures -- distance matrix, predecessors ..etc
		 
		distanceMatrix = new int[numVertices][numVertices]; //is the standard adjacency matrix to start with
		predecessorNode = new int[numVertices][numVertices]; //predecessorNode[a][b]=c => "c" is the penultimate vertex on the shortest path from a to b.
		edgeMatrix = new Edge[numVertices][numVertices]; //is like the adjacency matrix, but stores edge-objects instead of edge weights. 
		
		for ( int i =0; i< numVertices; i++){
			distanceMatrix[i][i] = 0; //distance to self is 0 - diagonal of the adjacency matrix
			edgeMatrix[i][i] = null;
		}
		//rest of the edges to infinity
		for (int i=0 ; i< numVertices; i++){
			for (int j=0; j< numVertices ; j++){
				distanceMatrix[i][j] = distanceMatrix[i][j] = Integer.MAX_VALUE;
				edgeMatrix[i][j] = null;
			}
		}
		//overwrite infinity with 1 , where edges are present
		for ( Edge e : graph.getEdges()){
			Vertex src = e.getGovernor();
			Vertex dst = e.getDependent();

			int i = mapNodeToId.get(src); 
			int j = mapNodeToId.get(dst);
			
			if ( sdp_vertices.contains(src) && sdp_vertices.contains(dst)){
				//this is a shortest dependency path vertex
				edgeWeights[i][j] = SDP_EDGE_WEIGHT;
				edgeWeights[j][i] = SDP_EDGE_WEIGHT;
			}
			
			//System.err.println("inserting edge info b/w " + i + " and " + j);
			distanceMatrix[i][j] = 1;
			if ( true== undirected_paths){
				 distanceMatrix[j][i] = 1;
			}

			//System.out.println("edge initialization to 1 of " + i + " to "+j);
			//the penultimate node in the path from i to j is i, whenever there is a direct edge
			// from i to j.
			predecessorNode[i][j] = i;   
			if ( true == undirected_paths){
				predecessorNode[j][i] = j;
			}

			edgeMatrix[i][j] = e;
			if ( true == undirected_paths) {
				edgeMatrix[j][i] = e;
			}
		}
		
		//step 2 of floyd warshall - run the K- loop
		// Now iterate over k.
		for (int k = 0; k < numVertices; k++) { // Compute Dk[i][j], for each i,j
			for (int i = 0; i < numVertices; i++) {
				for (int j = 0; j < numVertices; j++) {
					if (i != j) {
						int directPath = distanceMatrix[i][j];
						int kPath = distanceMatrix[i][k] + distanceMatrix[k][j];
						if (directPath == Integer.MAX_VALUE) {
							if (kPath > 0 && kPath < Integer.MAX_VALUE) {
								distanceMatrix[i][j] = kPath;
								predecessorNode[i][j] = predecessorNode[k][j];
							}
						} else if (directPath < Integer.MAX_VALUE && kPath > 0  && kPath < Integer.MAX_VALUE) {
							if (directPath > kPath) {
								predecessorNode[i][j] = predecessorNode[k][j];
								distanceMatrix[i][j] = kPath;
							}
						}
					}
				}
			}
		}
		
		
		//step 3 of Floyd Warshall - almost done - find the shortest paths recursively and update the path feature matrix
		pathFeatureMatrix = new HashMap< Integer, HashMap<Integer,  HashMap<String,Float> > > ();
		//initialize this first
		for ( int i =0; i < numVertices; i++){
			HashMap<Integer, HashMap<String,Float>> submap = new HashMap< Integer, HashMap<String,Float>>();
			for ( int j=0; j < numVertices; j++){
				submap.put(j,null) ; 
			}
			pathFeatureMatrix.put(i, submap);
		}
		//now fill it up with path features
		ArrayList<Path> all_pair_sp = new ArrayList<Path>();
		for ( int i =0; i < numVertices; i++){			
			Vertex src =  mapIdToNode.get(i);
			String srcLabel = src.getLabel();

			//note the j =i+1 part in the for loop below
			//only one of the paths (a to b ) or (b to a) is evaluated, as they are symmetrical
			for ( int j= i+1; j < numVertices; j++){ 
					Vertex dst =  mapIdToNode.get(j);
					String dstLabel = dst.getLabel();

					Path newPath = new Path();
					newPath.pathFeatures  =  get_path_features(i, j); 
					
					//given that only path from a to be is evaluated or b to a (also no a to a) , 
					//regard a as source and b as destination if a < b lexicographically.
					if ( srcLabel.compareTo(dstLabel) <= 0 ){ 
						newPath.srcLabel  = srcLabel;
						newPath.dstLabel  = dstLabel;	
					}else{
						newPath.srcLabel  = dstLabel;
						newPath.dstLabel  = srcLabel;
					}
					
					all_pair_sp.add(newPath);
			}			
		}
		
		
		//free up the datastructures used for computations
		this.distanceMatrix = null;
		this.edgeMatrix = null;
		this.mapNodeToId = null;
		this.mapIdToNode = null;
		this.pathFeatureMatrix = null;
		this.predecessorNode = null;
	
		//all done
		return all_pair_sp;
	}//end of the main compute routine
	
	
	//helper to add feature counts
	private static void multiply_feature_in_path( HashMap<String, Float> feature_map, String feature_name, Float feature_weight){
		Float feature_value = feature_weight;
		if ( feature_map.containsKey(feature_name)){
			feature_value =  feature_map.get(feature_name) * feature_weight;
		}

		feature_map.put(feature_name, feature_value);
	}
	
	//populate path level features into ret
	private HashMap<String, Float>  get_path_features(int i, int j) throws InvalidActivityException {
		HashMap<String, Float> path_features = pathFeatureMatrix.get(i).get(j) ;
		if ( path_features != null){
			return  path_features; // it is already computed
		} //else compute it and update path_feature_matrix
		
		path_features = new HashMap<String, Float>();
		//System.err.println(" 	populate_path_features " +  i + " to " + j);
		if ((i != j) && (distanceMatrix[i][j]) != Integer.MAX_VALUE ) { //Dk[i][j] is infinity when there is no path between them - disconnected graph?
			int pre_destination = predecessorNode[i][j];
			
			//recursively process the edge features  - only the last edge to be dealt here
			HashMap<String,Float> sub_path_features = get_path_features(i, pre_destination);
			path_features.putAll(sub_path_features);

			
			//extract the features on the last edge in this path fro i to j 
			Edge last_edge =  edgeMatrix[ pre_destination ][j];
			Float feature_weight =  edgeWeights[ pre_destination ][j];
			if ( null == last_edge ){
				System.err.println("edge initialize must have been broken .. APSPGraph.java:populate_path_features .. check again, when looking into" + predecessorNode[i][j] + " to " + j );
				throw new InvalidActivityException();
			}else{
				multiply_feature_in_path(path_features,  last_edge.getLabel(), feature_weight);
				//now find direction
				if ( (mapNodeToId.get(last_edge.getGovernor()) == pre_destination )  
						&& (mapNodeToId.get(last_edge.getDependent()) == j ) 
						)
				{
					multiply_feature_in_path(path_features,FORWARD_EDGE, feature_weight);
					multiply_feature_in_path(path_features,EDGE_DISTANCE, feature_weight);
				}else if ( (mapNodeToId.get(last_edge.getGovernor()) == j ) 
						&& (mapNodeToId.get(last_edge.getDependent()) == pre_destination ) 
						) {
					multiply_feature_in_path(path_features,BACKWARD_EDGE, feature_weight); //count of edges of certain direction
					multiply_feature_in_path(path_features,EDGE_DISTANCE, feature_weight); //just the plain count of edges
				}else{
					System.err.println("edge direction initialize has screwed up in floydwarshall.. check again edge b/w " + i + " to " +j );
					System.exit(-1);
				}
			}
		}//nothing to do if i ==j

		//cache this computation
		pathFeatureMatrix.get(i).put(j, path_features);
		
		return path_features; 
	}//end of function populate_path_features	

	//some helper functions - break a dependency string such as String teststr= "root(ROOT-0, use-2) amod(use-2, Inappropriate-1) case(carbamazepine-4, of-3) nmod(use-2, carbamazepine-4) cc(carbamazepine-4, and-5) conj(carbamazepine-4, vigabatrin-6) case(seizures-10, in-7) amod(seizures-10, typical-8) compound(seizures-10, absence-9) nmod(use-2, seizures-10) punct(use-2, .-11) ";
	//into its components
	public static void parseDependencyString(
			String dependencyString, List<String> edgeLabels 
			, List<String> governorWords, List<Integer> governorPositions
			, List<String> dependentWords, List<Integer> dependentPositions
			){
		int edgeCount =0;
		Pattern pat = Pattern.compile("(\\S+)\\((\\S+)-(\\d+), (\\S+)-(\\d+)\\)");
		Matcher mat = pat.matcher(dependencyString);
		while ( mat.find()){
			String edgeLabel  = mat.group(1);
			String govWord  = mat.group(2);
			String govPosition  = mat.group(3);
			String depWord  = mat.group(4);
			String depPosition  = mat.group(5);
			
			edgeLabels.add(edgeLabel);
			governorWords.add(govWord);
			governorPositions.add( Integer.parseInt(govPosition));
			dependentWords.add(depWord);
			dependentPositions.add( Integer.parseInt(depPosition));			
		}
	}
	//this is the inverse of the above function - parseDependencyString
	public static String formDependencyString(
			List<String> edgeLabels 
			, List<String> governorWords, List<Integer> governorPositions
			, List<String> dependentWords, List<Integer> dependentPositions
			){
		String dependencyString="";
		for (int edgeCounter = 0 ; edgeCounter < edgeLabels.size(); edgeCounter++){
			dependencyString+= (
							edgeLabels.get(edgeCounter)+"("
							+governorWords.get(edgeCounter)+"-"+governorPositions.get(edgeCounter)+", "
							+dependentWords.get(edgeCounter)+"-"+dependentPositions.get(edgeCounter)+") "							
					);
		}
		return dependencyString;
	}

	
	//break asmGraph string into its components -- its format is of the form "root(ROOT-0/ROOT, occur-13/VBD); det(...
	public static void parseASMGraphString(
			String asmGraphString, List<String> edgeLabels 
			, List<String> governorWords, List<Integer> governorPositions, List<String> governorPosTags
			, List<String> dependentWords, List<Integer> dependentPositions, List<String> dependentPosTags
			){
		//int edgeCount =0;
		Pattern pat = Pattern.compile("(\\S+)\\((\\S+)-(\\d+)/(\\S+), (\\S+)-(\\d+)/(\\S+)\\);");
		Matcher mat = pat.matcher(asmGraphString);
		while ( mat.find()){
			String edgeLabel  = mat.group(1);
			String govWord  = mat.group(2);
			String govPosition  = mat.group(3);
			String govPosTag  = mat.group(4);
			String depWord  = mat.group(5);
			String depPosition  = mat.group(6);
			String depPosTag	= mat.group(7);
			
			edgeLabels.add(edgeLabel);
			governorWords.add(govWord);
			governorPositions.add(Integer.parseInt(govPosition));
			governorPosTags.add(govPosTag);
			dependentWords.add(depWord);
			dependentPositions.add(Integer.parseInt(depPosition));
			dependentPosTags.add(depPosTag);
		}
	}
	
	public static String formASMGraphString(
			List<String> edgeLabels 
			, List<String> governorWords, List<Integer> governorPositions, List<String> governorPosTags
			, List<String> dependentWords, List<Integer> dependentPositions,List<String> dependentPosTags  
			){
		String asmGraphString="";
		for (int edgeCounter = 0 ; edgeCounter < edgeLabels.size(); edgeCounter++){
			asmGraphString+= (
							edgeLabels.get(edgeCounter)+"("
							+governorWords.get(edgeCounter)+"-"+governorPositions.get(edgeCounter)+ "/" + governorPosTags.get(edgeCounter)  + ", "
							+dependentWords.get(edgeCounter)+"-"+dependentPositions.get(edgeCounter)+"/"+ dependentPosTags.get(edgeCounter)  +"); "							
					);
		}
		return asmGraphString;
	}
	
	public static DirectedGraph<Vertex,Edge> createASMGraphWithLinearSubgraph(String asmGraphString){
		if ( asmGraphString.length() == 0){
			return new DirectedSparseGraph<Vertex,Edge>(); //empty string, empty graph 
		}
		
		DirectedGraph<Vertex,Edge> graph  = createASMGraph(asmGraphString);
		
		//include a subgraph based on linear ordering of tokens in the sentence
		ArrayList<Vertex> linearVertices = new ArrayList<Vertex>();
		
		//find the region containing the first and second entities in the linear order graph.
		Integer entityRegionStart = graph.getVertexCount(); 
		Integer entityRegionEnd = 0;
		ArrayList<Vertex> entityNodes = new ArrayList<Vertex>();
		for ( Edge ed : graph.getEdges()){
			Vertex gov = ed.getGovernor();
			Vertex dep = ed.getDependent();
			if ( gov.getWord().startsWith("Entity")){
				entityNodes.add(dep);
			}else if( dep.getWord().startsWith("Entity")){
				entityNodes.add(gov);
			} 
		}
		for( Vertex v : entityNodes){
			Integer vPos= v.getPosition();
			if (vPos < entityRegionStart){
				entityRegionStart = vPos;
			}
			if (vPos > entityRegionEnd){
				entityRegionEnd = vPos;
			}
		}
		
		int noOfTokens = graph.getVertexCount();
		//now form the linear vertices, along with the regional info
		for( Vertex v : graph.getVertices()){
			if ( false == v.getWord().startsWith("Entity")){ //skip the Entity -special nodes
				String regionSuffix= "";
				Integer vPos= v.getPosition();
				if (vPos <= entityRegionStart){
					regionSuffix = "Before_";
				}else if (vPos >= entityRegionEnd){
					regionSuffix = "After_";
				}
				String newVertexRep = 	 regionSuffix + v.getLemma()  
										+ "-" +  ( noOfTokens + v.getPosition()) 
										+ "/" + v.getTag() ;
				
				Vertex newLinearVertex = new Vertex(newVertexRep	);
				newLinearVertex.setLemma(regionSuffix + v.getLemma()  );
				newLinearVertex.setGeneralizedPOS(v.getGeneralizedPOS());
				newLinearVertex.setCompareForm(v.getCompareForm());
				linearVertices.add( newLinearVertex );
			}
		}
		//add the newly formed linear vertices into the main graph - connect them up too.
		Collections.sort(linearVertices, new Comparator<Vertex>(){
		     public int compare(Vertex o1, Vertex o2){
		         if(o1.getPosition() == o2.getPosition())
		             return 0;
		         return o1.getPosition() < o2.getPosition() ? -1 : 1;
		     }
		});	
		
		graph.addVertex(linearVertices.get(0));
		for ( int vidx = 1; vidx < linearVertices.size() ; vidx ++ ){
			graph.addVertex( linearVertices.get(vidx) );
			Vertex src =  linearVertices.get(vidx-1);
			Vertex dst =  linearVertices.get(vidx);
			
			Edge linEdge = new Edge(src, "Linear", dst);		    
			graph.addEdge(linEdge, src, dst);
		}
						
		return graph;
	}
	
	public static DirectedGraph<Vertex,Edge> createASMGraph(String asmGraphString){
		DirectedGraph<Vertex,Edge> graph = new DirectedSparseGraph<Vertex,Edge>();
		
		if ( asmGraphString.length() == 0){
			return graph; //empty string, empty graph 
		}
	
		List<String> edgeLabels = new ArrayList<String>();
		List<String> governorWords = new ArrayList<String>(); 
		List<Integer> governorPositions  = new ArrayList<Integer>();
		List<String> governorPosTags = new ArrayList<String>();
		List<String> dependentWords = new ArrayList<String>();
		List<Integer> dependentPositions = new ArrayList<Integer>();
		List<String> dependentPosTags = new ArrayList<String>();
		
		parseASMGraphString(asmGraphString, edgeLabels
				, governorWords, governorPositions, governorPosTags
				, dependentWords, dependentPositions, dependentPosTags
				);
		
		Map<Integer, Vertex> tokenToNode = new HashMap<Integer, Vertex>();
		for ( int edgeCounter = 0 ; edgeCounter < edgeLabels.size() ; edgeCounter++ ){
			Integer governorPosition = governorPositions.get(edgeCounter);
			Integer dependentPosition = dependentPositions.get(edgeCounter);

			Vertex governor;
			if ( tokenToNode.containsKey(governorPosition)){
				governor = tokenToNode.get(governorPosition);
			}else{
				String governorStringRep = 	governorWords.get(edgeCounter) 
												+ "-" +governorPositions.get(edgeCounter) 
												+ "/" + governorPosTags.get(edgeCounter); 
				governor = new Vertex(governorStringRep	);
				governor.setLemma( governorWords.get(edgeCounter) );
				governor.setGeneralizedPOS(governorPosTags.get(edgeCounter));
				governor.setCompareForm(governorStringRep);
				graph.addVertex(governor);
				tokenToNode.put(governorPosition, governor);
			}

			Vertex dependent;
			if ( tokenToNode.containsKey(dependentPosition)){
				dependent = tokenToNode.get(dependentPosition);
			}else{
				String dependentStringRep = 	dependentWords.get(edgeCounter) 
												+ "-" +dependentPositions.get(edgeCounter) 
												+ "/" + dependentPosTags.get(edgeCounter);
				dependent = new Vertex(dependentStringRep);
				dependent.setLemma( dependentWords.get(edgeCounter) );
				dependent.setCompareForm(dependentStringRep);
				graph.addVertex(dependent);
				tokenToNode.put(dependentPosition, dependent);
			}

			
			if ( governorPosition != dependentPosition){ //skip self edges or loop
				Edge govToDep = new Edge(governor, edgeLabels.get(edgeCounter), dependent);		    
				graph.addEdge(govToDep, governor, dependent);
			}

		}	
		return graph;
	}	
}

