package myutils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.activity.InvalidActivityException;

import edu.ucdenver.ccp.asm.Edge;
import edu.ucdenver.ccp.asm.Vertex;
import edu.uci.ics.jung.graph.DirectedGraph;
import it.uniroma2.sag.kelp.data.dataset.SimpleDataset;
import it.uniroma2.sag.kelp.data.example.Example;
import it.uniroma2.sag.kelp.data.representation.string.StringRepresentation;
import it.uniroma2.sag.kelp.kernel.DirectKernel;

// ASM kernel can be directly invoked from the kelp-classifier by specifying "asmspk-<name of data representation that holds the graph>".
// But running APSPGraph first to linearize the asm features, and then invoking the linear classifier from Kelp, is much faster.  


public class ASMSPK extends DirectKernel<StringRepresentation> {
	public boolean undirected_paths = true;
	private HashMap<Integer, HashMap<String,Float> > cacheForShortestPaths = new HashMap<Integer, HashMap<String,Float> >();

	private String wordForEntity1, wordForEntity2;
	private String asmGraphRepresentationName;
    
    private HashMap<Integer, Float> gramMatrixCache = new HashMap<Integer, Float>();
	
	public ASMSPK(String nameOfDataRepresentation, String snw, String enw) throws IOException {
		super(nameOfDataRepresentation);//have to inform the parent/base class to send me the field (which is |BS:asmGraph|..|ES|)
		this.asmGraphRepresentationName = nameOfDataRepresentation;
		this.wordForEntity1 = snw;
		this.wordForEntity2 = enw;
	}
	
	private static int kernelComputationCounter=0;

	//get shortest path computed for each graph objects
	private HashMap<String, Float> getASMFeatures( String graphStr ) throws InvalidActivityException{
		int key = graphStr.hashCode();
		if ( cacheForShortestPaths.containsKey(key))
		{
			return cacheForShortestPaths.get(key);
		}else{
			//as linearized flat features
			DirectedGraph<Vertex,Edge> graph = APSPGraph.createASMGraphWithLinearSubgraph( graphStr);
			Boolean use_undirected_paths = true;
			ArrayList<Path> lst_paths=  (new APSPGraph(wordForEntity1, wordForEntity2 )).get_all_pair_shortest_path(graph, use_undirected_paths);
			HashMap<String, Float> asm_flat_features = Path.get_asm_flatfeatures_from_allpaths(lst_paths);
	     	
			cacheForShortestPaths.put(key, asm_flat_features);
			int cachesize = cacheForShortestPaths.size();
			if ( cachesize %1000 ==0 ){
				System.err.println("in asmspk kernel compute. sp cache size is " + cacheForShortestPaths.size());
			}
			return asm_flat_features;
		}			

	}

	 
	
	@Override
	public float kernelComputation(StringRepresentation repA, StringRepresentation repB)  {
		HashMap<String, Float> featuresInA = null;
		HashMap<String, Float> featuresInB = null;
		String graphStrA = repA.getTextFromData();
		String graphStrB = repB.getTextFromData();
	
		Integer gramMatrixKey = getKernelMatrixKey(graphStrA, graphStrB);
		if ( gramMatrixCache.containsKey(gramMatrixKey)){
			return gramMatrixCache.get(gramMatrixKey);
		}
			
		try {
			featuresInA = getASMFeatures( graphStrA );
			featuresInB  =  getASMFeatures( graphStrB );	
		} catch (InvalidActivityException e) {
			e.printStackTrace();
		} 
		
		Float finalKernelValue = 0.0f;
		
		for ( String featureName  : featuresInA.keySet()){
			if (featuresInB.containsKey(featureName) ){
				finalKernelValue +=  ( featuresInA.get(featureName) * featuresInB.get(featureName));
			}
		}
		
		//System.out.println("asmspk: kernel computation value=" + final_kernel_value + " for " + repA.getTextFromData() + " " + repB.getTextFromData());
		if ( (kernelComputationCounter++  % 1000000) == 0 ){
			System.err.println("in asmspk. no of kernel computations =" + (kernelComputationCounter/1000000) + "x M  and cache size is" + cacheForShortestPaths.size() );
		}
		
		gramMatrixCache.put(gramMatrixKey, finalKernelValue);
		
		return finalKernelValue;
	}

	
	private Integer getKernelMatrixKey(String graphStrA, String graphStrB){
		String gramMatrixCacheKey =null ;
		if ( graphStrA.compareTo(graphStrB)  <0 ){
			gramMatrixCacheKey =  graphStrA + "_arg2_" + graphStrB;
		}else{
			gramMatrixCacheKey =  graphStrB + "_arg2_" + graphStrA;
		}
		return gramMatrixCacheKey.hashCode();
	}

	public void populateGramMatrix (SimpleDataset dataset) throws Exception {
		int progress =0;
        int noOfExamples = dataset.getNumberOfExamples();
		for ( int i =0; i< noOfExamples; i++){
            Example exA = dataset.getExample(i);
			String graphA = exA.getRepresentation(this.asmGraphRepresentationName).getTextFromData();
			
			for ( int j= i; j < noOfExamples; j++){
                Example exB = dataset.getExample(j);
				String graphB = exB.getRepresentation(this.asmGraphRepresentationName).getTextFromData();
					
				Float kernelValue = this.kernelComputation(exA, exB);
				gramMatrixCache.put( getKernelMatrixKey(graphA, graphB), kernelValue);
				
				if ( (progress++ % 1000000 ) ==0 ){
					System.err.println("populateGramMatrix progress " + progress/1000000 + " M");
				}
				
			}
		}		
		System.err.println("populated gram matrix cache for the examples dataset." + gramMatrixCache.size() +" done");
	}
    
	public void saveGramMatrixToFile(String gramMatrixCacheFile) throws Exception {
		System.err.println("Saving gram matrix to file " + gramMatrixCacheFile);
		PrintWriter pw = new PrintWriter(new FileWriter(gramMatrixCacheFile));
		for ( Integer key : gramMatrixCache.keySet()){
			pw.println( key + " " + gramMatrixCache.get(key));
		}
		pw.close();
		System.err.println("done");
	}
	
	public void populateGramMatrixFromFile(String gramMatrixCacheFile) throws Exception {
		BufferedReader inReader;
		try {
			inReader = new BufferedReader( new InputStreamReader( new FileInputStream(gramMatrixCacheFile)));
			String line= null;
			while( (line=inReader.readLine())!=null ){
				String[] fields = line.split(" ");
				gramMatrixCache.put(
										Integer.parseInt(fields[0]),
										Float.parseFloat(fields[1])
								);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("populated gram matrix cache from file.."+  gramMatrixCache.size() +" done");
	}

	public static void main(String[] args) throws Exception {
		if (args.length < 2 ){
			System.err.println("Args required: <examples file> <gram matrix cache file (output) >   <gram matrix cache file input> " ); 
      	}else{
			String examplesFile =  args[0];
			String gramMatrixCacheFile = args[1];
			SimpleDataset dataset = new SimpleDataset();
			dataset.populate(examplesFile);

			String asmGraphRepresentationName = "asmGraph_with_entities";
			ASMSPK asmspk = new ASMSPK(asmGraphRepresentationName , "Entity1", "Entity2");

            if ( args.length > 2 ){
                String preComputedGMCacheFile = args[2];
                asmspk.populateGramMatrixFromFile(preComputedGMCacheFile);
            }

			asmspk.populateGramMatrix(dataset);
			asmspk.saveGramMatrixToFile(gramMatrixCacheFile);
		}
	}

}
