package myutils;

import java.util.ArrayList;

enum EntityType{
	CHEMICAL, DISEASE;
}



/*
 * To represent a relation - entity pair
 */

//class EntityPair{
//	Entity chemical;
//	Entity disease;
//	Boolean relation; // true if this pair is a relation
//}

class Relation{
	String chemical;
	String disease; // the pmids of chemical and disease entities
	public Relation(String pc, String pd) {
		chemical = pc;
		disease = pd;
	}
	@Override
	public boolean equals(Object otherObj){
		if (otherObj instanceof Relation){
			Relation other = (Relation)otherObj;
			return   this.chemical.contentEquals(other.chemical) && this.disease.contentEquals(other.disease);
		}else{
			return false;
		}
	}
	@Override
	public int hashCode(){
		return (chemical + ":" + disease).hashCode();
	}
}

/*
 * To represent CDR entities- Chemicals and Diseases
 */
public class Entity {

	String entity_id=""; String meshid=""; String pmid=""; String details="";
	int start=0; int end =0 ;  int sentenceId=  -1;
//	int sentence_offset =0;
	EntityType type;

	public static String type_to_string(EntityType t){
		String str="";

		switch(t){
		case CHEMICAL:
			str= "Chemical";
			break;

		case DISEASE:
			str= "Disease";
			break;

		default:
			throw new RuntimeException("check type to string");
		}
		return str;
	}
	
	public static EntityType  string_to_type(String str){
		if (str.contentEquals("Chemical")){
			return EntityType.CHEMICAL;
		}else if (str.contentEquals("Disease")){
			return EntityType.DISEASE;
		}else{
			throw new RuntimeException("check string to type ");
		}			
	}
	
	public Entity( String pmid, String start, String end, String details, String type, String meshid){
		this.meshid 	= meshid;
		this.pmid 		= pmid;
		this.details	= details;
		this.start		= Integer.parseInt(start);
		this.end		= Integer.parseInt(end);
		this.type 		= string_to_type(type); 
		this.entity_id 	= pmid + "-" + meshid + "-" + start;
		//this.sentence_offset = -1;
		this.sentenceId = 0;
	}
	
	public Integer getStartOffsetInDocument(){
		return start;	
	}
	public Integer getEndOffsetInDocument(){
		return end;	
	}
	
	public String to_string(){
		return String.join("@",
				new String[]{
								this.entity_id, this.meshid, this.details, type_to_string(this.type), 
								Integer.toString(start), Integer.toString(end), Integer.toString(this.sentenceId),
								this.pmid
							}				
		);
	}
	public static Entity fromString(String str){
		String[] fields = str.split("@");
		String meshid = fields[1];
		String details = fields[2];
		String type = fields[3]; 
		String start = fields[4];
		String end   = fields[5];
		String sentence_start = fields[6];
		String pmid = fields[7];
		
		Entity ent = new Entity(pmid, start, end, details, type, meshid);
		ent.sentenceId = Integer.parseInt(sentence_start);
		
		return ent;
	}


	public static ArrayList<Entity> parse_cdr_line(String line){
		ArrayList<Entity> ent_list =  new ArrayList<Entity>();
		String[] fields = line.trim().split("\t");//6794356    688    706    cardiac arrhythmia    Disease    D001145
        if (fields.length == 6){
        	ent_list.add( 
        					new Entity(fields[0], fields[1], fields[2], fields[3], fields[4], fields[5]
        				)
        			);
        }else{
         	String[] mesh_ids = fields[5].split("\\|");
        	String[] details_lst = fields[6].split("\\|");
        	
        	for (int idx =0; idx < mesh_ids.length; idx++){
        		ent_list.add( 
        			new Entity(fields[0], fields[1], fields[2], details_lst[idx], fields[4], mesh_ids[idx])
        			);
        	}
        }
		return ent_list;
	}	 	
}
