package myutils;


//main file to call kelp classification..

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.List;

import it.uniroma2.sag.kelp.data.dataset.SimpleDataset;
import it.uniroma2.sag.kelp.data.example.Example;
import it.uniroma2.sag.kelp.data.label.Label;
import it.uniroma2.sag.kelp.data.label.StringLabel;
import it.uniroma2.sag.kelp.kernel.Kernel;
import it.uniroma2.sag.kelp.kernel.cache.FixIndexKernelCache;
import it.uniroma2.sag.kelp.kernel.cache.FixIndexSquaredNormCache;
import it.uniroma2.sag.kelp.kernel.standard.LinearKernelCombination;
import it.uniroma2.sag.kelp.kernel.standard.NormalizationKernel;
import it.uniroma2.sag.kelp.kernel.tree.PartialTreeKernel;
import it.uniroma2.sag.kelp.kernel.tree.SubSetTreeKernel;
import it.uniroma2.sag.kelp.kernel.tree.SubTreeKernel;
import it.uniroma2.sag.kelp.kernel.vector.LinearKernel;
import it.uniroma2.sag.kelp.learningalgorithm.classification.libsvm.BinaryCSvmClassification;
import it.uniroma2.sag.kelp.learningalgorithm.classification.multiclassification.OneVsAllLearning;
import it.uniroma2.sag.kelp.predictionfunction.classifier.ClassificationOutput;
import it.uniroma2.sag.kelp.predictionfunction.classifier.Classifier;
import it.uniroma2.sag.kelp.utils.evaluation.BinaryClassificationEvaluator;
import it.uniroma2.sag.kelp.utils.evaluation.MulticlassClassificationEvaluator;
import javassist.expr.Instanceof;


	public class KelpClassifier {
		//parameters
		private static float cValue=0;
		private static float cPositiveValue=0;
		private static float cNegativeValue=0;
		private static String startNodeInGraph = "Entity1";
		private static String endNodeInGraph = "Entity2";
		private static String trainFileName = "";
		private static String testFileName  = "";
		private static String kernelAndkelpFieldName = "asmSpk-asmGraph_with_entities";
		private static StringLabel positiveLabel =null;
		private static StringLabel negativeLabel =null;
		
		private static void processArgs(String[] args){
			if ( args.length < 5){
				System.err.println("Arguments required: "
						+ "<Train File Name>  <Test FileName>  <kernel and kelp field names> "
						+ "<log of cvalue> <positive class name> <negative class name>" //mandatory argument
						+ "[cp value] [cn value]" );// optional
				System.exit(-1);
			}else{
				trainFileName = args[0];
				testFileName = args[1];
				kernelAndkelpFieldName = args[2];
				cValue = (float) Math.pow(2, Double.parseDouble(args[3]));
				positiveLabel = new StringLabel( args[4] );
				negativeLabel = new StringLabel( args[5] );
                if (args.length > 6 ){
                	cPositiveValue = (float) Math.pow(2, Double.parseDouble(args[6]));
                	cNegativeValue = (float) Math.pow(2, Double.parseDouble(args[6]));
                }
			}	
		}
		
		public static void main(String[] args) {
			processArgs(args);

			try {
				System.err.println("Reading Train and Test Sets...");
				SimpleDataset trainingSet = new SimpleDataset();
				trainingSet.populate(trainFileName);
				SimpleDataset testSet = new SimpleDataset();
				testSet.populate(testFileName);

				Kernel kernelFunction  = getKernelFunction(trainingSet,  testSet, kernelAndkelpFieldName);
				System.out.println("Kernel to be used "+ kernelAndkelpFieldName);

				//Training set up 
				System.err.println("Training starts..");
				Classifier clf =  getSVMPredictionFunction(kernelFunction, trainingSet, positiveLabel); 

				
				//Start Predicting now
				String predictionFileName =  testFileName + ".kelp.predictions." + kernelAndkelpFieldName;
				predictTestSet(clf,  trainingSet, testSet, predictionFileName);
			
                	
				
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			System.err.println("Done");
		}
		
		public static void predictTestSet(Classifier clf, SimpleDataset trainingSet, SimpleDataset testSet, String predictionsFileName) throws Exception{
			System.err.println("classification done.. writing the predictions out now");
			//MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator(trainingSet.getClassificationLabels());
			BinaryClassificationEvaluator evaluator = new BinaryClassificationEvaluator(positiveLabel);
			
			//classify the test samples
			PrintWriter pw = new PrintWriter(new FileWriter(predictionsFileName)); //writing out predictions
			for (Example e : testSet.getExamples()) {
				ClassificationOutput p = clf.predict(e); // Predict the class
				
				evaluator.addCount(e, p);
				StringLabel predictedLabel = null;
				
				if (p.getScore(positiveLabel) >= 0){
					predictedLabel = positiveLabel;
				}else{
					predictedLabel = negativeLabel;
				}
				
//				List<Label> predictedClasses = p.getPredictedClasses();
//				if ( 1 != predictedClasses.size() )
//				{
//					pw.close();
//					throw new  UnexpectedException("Was expecting to have only one label from predictions??. In KelpClassifier.");
//				}else{
//					Label predictedLabelObj =  predictedClasses.get(0);
//					predictedLabel = predictedLabelObj.toString();
//				}
				String classificationScores= "";
				for ( Label classtype: p.getAllClasses()){
					classificationScores += ( " " + classtype.toString() +":" +  p.getScore(classtype) );
				}

				String originalLabel = e.getLabels()[0].toString();
				String exampleString =  e.toString();
				exampleString = exampleString.replaceFirst(originalLabel, predictedLabel.toString()); //replace the first part - originalLabel with predictedLabel
				pw.write( exampleString );
				pw.write("|BS:actualLabel|" + originalLabel + "|ES|");
				pw.write("|BS:classificationScores|" + classificationScores + "|ES|");
				pw.write("\n");
			
				int progressMarker = 0;
				int totalExamplesProcessed = 0;
				totalExamplesProcessed ++;
				if (progressMarker++  > 100){
					progressMarker = 0;
					System.err.println("writing out predictions. Total examples progressed " + totalExamplesProcessed );
				}
			}
			pw.close();
			System.out.println("Prediction Accuracy=" + evaluator.getAccuracy() + " prec= " + evaluator.getPrecision() + " recall=" + evaluator.getRecall());
			System.err.println("Prediction Accuracy=" + evaluator.getAccuracy() + " prec= " + evaluator.getPrecision() + " recall=" + evaluator.getRecall());
		}
		
		public static  Kernel getASMSPKKernel(int cacheSize , String asmGraphRepresentationName, String startNodeWord, String endNodeWord) throws Exception{
			ASMSPK asm_spk = new ASMSPK(asmGraphRepresentationName, startNodeWord, endNodeWord );
            asm_spk.setSquaredNormCache(new FixIndexSquaredNormCache(cacheSize));
			return new NormalizationKernel(asm_spk);
		}

		public static  Kernel getAPGKernel(int cacheSize ,String asmGraphRepresentationName, String startWord, String endWord) throws IOException{
			APGKernel apgk = new APGKernel(asmGraphRepresentationName, startWord, endWord);
			apgk.setSquaredNormCache(new FixIndexSquaredNormCache(cacheSize));
			return new NormalizationKernel(apgk);
		}

		
		public static  Kernel getLinearKernel(int cacheSize, String representationName){
			Kernel linearKernel = new LinearKernel(representationName); //assumes vector representation name is bow	
			linearKernel.setSquaredNormCache(new FixIndexSquaredNormCache(cacheSize));
			return new NormalizationKernel(linearKernel);
		}
		public static Kernel getSubTreeKernel(int cacheSize, String treeRepresentationName){
			Kernel stk = new SubTreeKernel(0.4f, treeRepresentationName);
			stk.setSquaredNormCache(new FixIndexSquaredNormCache(cacheSize));
			return new NormalizationKernel(stk);
		}
		public static Kernel getSubSetTreeKernel(int cacheSize, String treeRepresentationName){
			Kernel stkgrct = new SubSetTreeKernel(0.4f, treeRepresentationName);
			stkgrct.setSquaredNormCache(new FixIndexSquaredNormCache(cacheSize));
			return new NormalizationKernel(stkgrct);
		}

		public static Kernel getPartialTreeKernel(int cacheSize, String treeRepresentationName){
			Kernel ptkgrct = new PartialTreeKernel(0.4f, 0.4f, 5f, treeRepresentationName);
			ptkgrct.setSquaredNormCache(new FixIndexSquaredNormCache(cacheSize));
			Kernel normPtkGrct = new NormalizationKernel(ptkgrct);
			return  normPtkGrct;
		}

		
		
		public static Kernel getKernelCombo(List<Kernel> listOfKernels){
			if (listOfKernels.size() <=0 ){
				return null;
			}else if (listOfKernels.size()==1)
			{
				return listOfKernels.get(0);
			}else {//combine them with equal weights
				LinearKernelCombination kercombo = new LinearKernelCombination();
				for ( Kernel ker : listOfKernels ){
					kercombo.addKernel( (float)1/listOfKernels.size(), ker);
				}
				kercombo.normalizeWeights();
				return kercombo;
			}
		}
		
		public static Kernel getKernelFunction(SimpleDataset trainingSet, SimpleDataset testSet, 
					String kernelAndFieldName)	throws Exception {

			int cacheSize = trainingSet.getNumberOfExamples();
			Kernel finalKernelToReturn = null;

			
			List<Kernel> listOfKernels = new ArrayList<Kernel>();
			
			for ( String kernel : kernelAndFieldName.split(":")){ // //linear-bow:stk-ptk:asm-asm_graphplus ..so on
				String kernelName = kernel.split("-")[0];
				String fieldName = kernel.split("-")[1];
				Kernel kernelFunction = null;
				
				if (kernelName.contentEquals("linear")){
					kernelFunction = getLinearKernel(cacheSize, fieldName);
				}else if (kernelName.equalsIgnoreCase("stk")) {
					kernelFunction= getSubTreeKernel(cacheSize, fieldName); 
				}else if (kernelName.equalsIgnoreCase("sstk")) {
					kernelFunction= getSubSetTreeKernel(cacheSize, fieldName); 
				}else if (kernelName.equalsIgnoreCase("ptk")) {
					kernelFunction= getPartialTreeKernel(cacheSize, fieldName);
				}else if (kernelName.equalsIgnoreCase("asmspk")){
					kernelFunction = getASMSPKKernel(cacheSize, fieldName, "Entity1", "Entity2");
				}else if (kernelName.equalsIgnoreCase("apg")){
					kernelFunction= getAPGKernel(cacheSize, fieldName, startNodeInGraph, endNodeInGraph);
				}else{
					System.err.println("unrecognized kernel name " + kernelName);
					System.exit(-1);
				}
				if ( kernelFunction!= null){
					listOfKernels.add(kernelFunction);
				}
			}

			if (0 == listOfKernels.size()){
				System.out.println("no proper kernelId" + kernelAndFieldName);
				System.exit(-1);
			}
	
			finalKernelToReturn = getKernelCombo(listOfKernels);
			finalKernelToReturn.setKernelCache(new FixIndexKernelCache(cacheSize));
			return finalKernelToReturn;
		}
		
		
		public static Classifier getSVMPredictionFunction(Kernel usedKernel, SimpleDataset trainingSet, StringLabel positiveLabel) {
			// instantiate an svmsolver
			BinaryCSvmClassification svmSolver = new BinaryCSvmClassification();
			svmSolver.setLabel( positiveLabel);
			svmSolver.setKernel(usedKernel);
			svmSolver.setC(cValue);
			svmSolver.setFairness(true);
			svmSolver.learn(trainingSet);
			Classifier f = svmSolver.getPredictionFunction();
			
			return f;
		}

	}

	

