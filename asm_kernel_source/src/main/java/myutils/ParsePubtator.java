package myutils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activity.InvalidActivityException;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.BasicDependenciesAnnotation;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.EnhancedDependenciesAnnotation;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.EnhancedPlusPlusDependenciesAnnotation;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;
import edu.ucdenver.ccp.asm.Edge;
import edu.ucdenver.ccp.asm.Vertex;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import it.uniroma2.sag.kelp.data.representation.string.StringRepresentation;

public class ParsePubtator {

	public static void main(String[] args) throws IOException {
		if ( (args.length < 2) || (!args[1].endsWith("/")) ) {
			System.err.println("Arguments required <Pubtator_File>  <Output Directory For Parsed File>" );
		}else{
			pubtatorToKelp(args[0], args[1]);
		}		
	}
	
	public static void pubtatorToKelp(String cdr_file, String directoryForParsedFiles) throws IOException{
	    //collect all the information in the cdr file into 3 structures first  
	    HashMap<String, String>document_content= new HashMap<String,String>();// map of pubmed document to its text 
	    HashMap<String, ArrayList<Entity>>document_entities= new HashMap<String, ArrayList<Entity>>(); //map of pubmed document to the list of entities in it
	    HashMap<String, HashSet<Relation> >document_relations= new HashMap<String, HashSet<Relation>>() ; //map of pubmed document to the list of relations in it
	    
	    BufferedReader br = new BufferedReader(new FileReader(cdr_file));
	     
	    for (String line = br.readLine(), pmid="", title="", abstr="" ;
	    			line != null; line = br.readLine()) {
	    	  //pubmed document's attributes - abstr is document abstract
				
	    	if (line.contains("|t|") ) {
	    		String[] fields = line.split("\\|t\\|");
	    		pmid = fields[0];
	    		title = fields[1];  
	    	}else if  ( line.contains("|a|") ){
	    		abstr = line.split("\\|a\\|")[1];
	    		String full_text = title + " " +abstr;
	    		document_content.put(pmid, full_text);
	    	}else if (line.contains("\tCID\t")){
	    		String[] fields = line.split("\t");
	    		if (false == document_relations.containsKey(pmid)){
	    			document_relations.put(pmid, new HashSet<Relation>());
	    		}
	    		document_relations.get(pmid).add( new Relation(fields[2], fields[3]) );
	    	}else if ( line.trim().length() >0 ){
	    		//String[] fields = line.trim().split("\t");//6794356    688    706    cardiac arrhythmia    Disease    D001145
	    		ArrayList<Entity> entlst = Entity.parse_cdr_line(line);
	    		if ( false == entlst.isEmpty()){
	    			if (false == document_entities.containsKey(pmid)){
	    				document_entities.put( pmid, new ArrayList<Entity>() );
	    			}
	    			document_entities.get(pmid).addAll(entlst);
	    		}
	    	}else if (line.trim().length() == 0 ){// safe to skip - end of pmid document
	    		continue;
	    	} else{ 
	    		System.err.println("Could not parse this line " + line );
	    		System.exit(-1);
	    	}	            
	    }
	    
		Properties props = new Properties();
		props.put( "annotators", "tokenize, ssplit, pos, lemma, parse, depparse" );
		StanfordCoreNLP pipeline = new StanfordCoreNLP( props );

		String fileForSentenceLevelExamples = directoryForParsedFiles+ "SentenceExamples.txt";
		String fileForNonSentenceExamples = directoryForParsedFiles+ "NonSentenceExamples.txt";
		String fileForGoldSentenceRelations = directoryForParsedFiles+ "GoldSentenceRelations.txt";
		String fileForGoldNonSentenceRelations = directoryForParsedFiles+ "GoldNonSentenceRelations.txt";

		
		PrintWriter fhSentenceExamples = new PrintWriter(new FileWriter(fileForSentenceLevelExamples));
		PrintWriter fhNonSentenceExamples = new PrintWriter(new FileWriter(fileForNonSentenceExamples));
		PrintWriter fhGoldSentenceRelations = new PrintWriter(new FileWriter(fileForGoldSentenceRelations));
		PrintWriter fhGoldNonSentenceRelations = new PrintWriter(new FileWriter(fileForGoldNonSentenceRelations));
		
		
		int no_of_pmids_processed = 0;
	    for (String pmid : document_content.keySet() ) {
	    	HashMap<Relation, Boolean> sentenceLevelGoldRelations = parse_pubmed_document(pmid, document_content.get(pmid), document_entities.get(pmid), document_relations.get(pmid), pipeline, fhSentenceExamples, fhNonSentenceExamples);
	    	for ( Relation rel : sentenceLevelGoldRelations.keySet()){
	    		if ( true == sentenceLevelGoldRelations.get(rel)){
	    			fhGoldSentenceRelations.println(pmid + ":" + rel.chemical + ":" + rel.disease);
	    		}else{
	    			fhGoldNonSentenceRelations.println(pmid + ":" + rel.chemical + ":" + rel.disease);
	    		}
	    	}
	    	
	    	
	    	if ((no_of_pmids_processed++ % 10)==0){
	    		System.err.println("done processing " + no_of_pmids_processed);
	    	}
	    }
	    
	    fhSentenceExamples.close();
	    fhNonSentenceExamples.close();
	    fhGoldSentenceRelations.close();
	    fhGoldNonSentenceRelations.close();
	}
	
	public static HashMap<Relation, Boolean> parse_pubmed_document(String pmid, String content, ArrayList<Entity> entlst,
		HashSet<Relation> goldRelations, StanfordCoreNLP pipeline, 
		PrintWriter fhSentenceExamples, PrintWriter fhNonSentenceExamples) throws InvalidActivityException{
		String input_text   = content.replace("\n", " ");
		Annotation document = new Annotation(input_text);
		pipeline.annotate(document);

	    //associate entities to the sentences in which each they fall 
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (Integer sentenceId=0 ; sentenceId < sentences.size();  sentenceId ++){ 
			CoreMap sentence = sentences.get(sentenceId);
			String sentence_text = sentence.toString();
			ArrayList<CoreLabel> tokens = new ArrayList<CoreLabel>( sentence.get(TokensAnnotation.class) );
			ArrayList<Integer> token_start_boundaries = new ArrayList<Integer>();
			ArrayList<Integer> token_end_boundaries = new ArrayList<Integer>();
			for( CoreLabel token : tokens){
				token_start_boundaries.add( token.get(CoreAnnotations.CharacterOffsetBeginAnnotation.class));
				token_end_boundaries.add(   token.get(CoreAnnotations.CharacterOffsetEndAnnotation.class));
			}
			Integer sentence_start = token_start_boundaries.get(0); //the first tokens start, is the same as the sentence start offset
			Integer sentence_end =   token_end_boundaries.get(token_end_boundaries.size()-1); //the first tokens start, is the same as the sentence start offset
			for( Entity ent : entlst ){
				if ((ent.start >= sentence_start) && (ent.end <= sentence_end) ){
					ent.sentenceId = sentenceId;
				}
			}
		}
		
		//first form sentence level examples
		HashSet<Relation> candidateSentenceLevelRelations = new HashSet<Relation>();
		for ( Entity ec : entlst){
			for (Entity ed : entlst){
				if (	(ec.type==EntityType.CHEMICAL) 	&& (ed.type == EntityType.DISEASE)	
					&& (ec.sentenceId == ed.sentenceId)
					)
				{ 	//this is a sentence level entity pair
					Relation rel = new Relation(ec.meshid, ed.meshid);
					candidateSentenceLevelRelations.add(rel);
					String label ="NOT_RELATED";
					if (goldRelations.contains(rel)){ //hash set look up is failing -- obje
						label = "CID";
					}
					formSentenceLevelExamples(label, ec, ed, sentences.get(ec.sentenceId),fhSentenceExamples);
				}	
			}
		}
		
		//another pass to extract non-sentence- level relations. If a entity pair is is already covered by sentence level examples, then skip those
		for ( Entity ec : entlst){
			for (Entity ed : entlst){
				if ( (ec.type==EntityType.CHEMICAL) && (ed.type == EntityType.DISEASE)
						&& (ec.sentenceId != ed.sentenceId)
					)
				{ 	//this is a sentence level entity pair
					Relation rel = new Relation(ec.meshid, ed.meshid);
					if ( false ==candidateSentenceLevelRelations.contains(rel)){ 
						String label ="NOT_RELATED";
						if (goldRelations.contains(rel)){ //hash set look up is failing -- obje
							label = "CID";
						}
						formNonSentenceLevelExamples(label, ec, ed, sentences,fhNonSentenceExamples);
					}
				}	
			}
		}
		
		//split the gold relations into sentence and non sentence level - required for performance measurement later
		HashMap<Relation, Boolean> isGoldRelationSentenceLevel  = new HashMap<Relation, Boolean>();
		for ( Relation rel : goldRelations){
			if (candidateSentenceLevelRelations.contains(rel)){
				isGoldRelationSentenceLevel.put(rel, true);
			}else{
				isGoldRelationSentenceLevel.put(rel, false);
			}
		}
		return isGoldRelationSentenceLevel;
	}
	
	private static void formNonSentenceLevelExamples(String label, Entity ec, Entity ed, 
			List<CoreMap> sentences, PrintWriter fhNonSentenceExamples) throws InvalidActivityException{
		
		CoreMap chemicalSentence = sentences.get(ec.sentenceId);
		CoreMap diseaseSentence =  sentences.get(ed.sentenceId);
		
		ArrayList<CoreLabel> chemicalSentenceTokens = new ArrayList<CoreLabel>( chemicalSentence.get(TokensAnnotation.class) );
		ArrayList<CoreLabel> diseaseSentenceTokens = new ArrayList<CoreLabel>( diseaseSentence.get(TokensAnnotation.class) );
		
		ArrayList<String> chemicalLemmas = new ArrayList<String>();
		ArrayList<String> diseaseLemmas = new ArrayList<String>();
		ArrayList<Integer> chemicalTokenBoundaries = new ArrayList<Integer>();
		ArrayList<Integer> diseaseTokenBoundaries = new ArrayList<Integer>();
		ArrayList<String> chemicalPosTags = new ArrayList<String>();
		ArrayList<String> diseasePosTags = new ArrayList<String>();
		
		for( CoreLabel chemicalToken : chemicalSentenceTokens){
			chemicalLemmas.add( chemicalToken.get(CoreAnnotations.LemmaAnnotation.class));
			chemicalPosTags.add(chemicalToken.get(CoreAnnotations.PartOfSpeechAnnotation.class) );
			chemicalTokenBoundaries.add(chemicalToken.get(CoreAnnotations.CharacterOffsetEndAnnotation.class));
		}
		
		for( CoreLabel diseaseToken : diseaseSentenceTokens){
			diseaseLemmas.add( diseaseToken.get(CoreAnnotations.LemmaAnnotation.class));
			diseasePosTags.add(diseaseToken.get(CoreAnnotations.PartOfSpeechAnnotation.class) );
			diseaseTokenBoundaries.add(diseaseToken.get(CoreAnnotations.CharacterOffsetEndAnnotation.class));
		}
		
		fhNonSentenceExamples.print(label);
		fhNonSentenceExamples.print("|BS:pmid|" 				+ ec.pmid + "|ES|" );
		fhNonSentenceExamples.print("|BS:sentenceId_ec|" 			+ ec.sentenceId+ "|ES|" );
		fhNonSentenceExamples.print("|BS:sentenceId_ed|" 			+ ed.sentenceId+ "|ES|" );
		fhNonSentenceExamples.print("|BS:sentence_ec|" 			+ chemicalSentence.toString() + "|ES|" );
		fhNonSentenceExamples.print("|BS:sentence_ed|" 			+ diseaseSentence.toString() + "|ES|" );
		fhNonSentenceExamples.print("|BS:entity1|" 		    + ec.to_string() + "|ES|" );
		fhNonSentenceExamples.print("|BS:entity2|" 		    + ed.to_string() + "|ES|" );
		fhNonSentenceExamples.print("|BS:lemmas_ec|" 				+ String.join(" " , chemicalLemmas) + "|ES|" );
		fhNonSentenceExamples.print("|BS:lemmas_ed|" 				+ String.join(" " , diseaseLemmas) + "|ES|" );

		//parse structures - constituency Parse
		String constituencyParse =  "(DOCROOT " 
											+chemicalSentence.get(TreeAnnotation.class).toString()
											+diseaseSentence.get(TreeAnnotation.class).toString()
											+ ")" ;
		fhNonSentenceExamples.print("|BT:cpTree|"				+ constituencyParse + "|ET|" );  

		
		//dependency parse 
		SemanticGraph chemicalBasicDependencies= chemicalSentence.get(BasicDependenciesAnnotation.class);
		SemanticGraph diseaseBasicDependencies=  diseaseSentence.get(BasicDependenciesAnnotation.class);
		
		//dependency parse in LCT format
		String chemicalLct =  get_dp_edges_in_parenthesis(chemicalBasicDependencies, chemicalLemmas, 
											chemicalPosTags,  chemicalTokenBoundaries
											,ec.getStartOffsetInDocument(), ec.getEndOffsetInDocument() 
											,-1, -1 
											,chemicalPosTags);
		String diseaseLct =  get_dp_edges_in_parenthesis(diseaseBasicDependencies, diseaseLemmas 
											,diseasePosTags,  diseaseTokenBoundaries
											,ed.getStartOffsetInDocument(), ed.getEndOffsetInDocument() 
											,-1, -1 
											,diseasePosTags);
		String lct		= "(LEX##DOCROOT::docroot " + chemicalLct + diseaseLct + " )";
		fhNonSentenceExamples.print("|BT:lct|" 	+ lct 	+ "|ET|" );


		//asm graph from enhanced dependencies
		SemanticGraph chemicalEnhancedDependencies= chemicalSentence.get(EnhancedDependenciesAnnotation.class);
		SemanticGraph diseaseEnhancedDependencies= diseaseSentence.get(EnhancedDependenciesAnnotation.class);

		
		fhNonSentenceExamples.print("|BS:dpPlusGraph_ec|" 		+ chemicalEnhancedDependencies.toList().replaceAll("\n", " ")+ "|ES|" );
		fhNonSentenceExamples.print("|BS:dpPlusGraph_ed|"		+ diseaseEnhancedDependencies.toList().replaceAll("\n", " ") + "|ES|" );

		boolean include_entities= true;
		String chemicalASMGraphString = get_asm_format(chemicalEnhancedDependencies, chemicalLemmas, chemicalPosTags, chemicalTokenBoundaries 
				,ec.getStartOffsetInDocument(), ec.getEndOffsetInDocument() 
				,-1,-1
				,chemicalPosTags, include_entities);
		String diseaseASMGraphString = get_asm_format(diseaseEnhancedDependencies, diseaseLemmas, diseasePosTags, diseaseTokenBoundaries 
				,-1,-1
				,ed.getStartOffsetInDocument(), ed.getEndOffsetInDocument()
				,diseasePosTags, include_entities);
		
		
		//advance the token positions in disease -sentence by no of tokens in chemical string , and then combine the two
		//asm graphs into a single asm graph, with a special edge between the roots of the two graphs  
		int noOfTokensInChemicalSentence = chemicalLemmas.size() +2; //the additional 2 is for the special nodes like Entity1, Entity2
		List<String> edgeLabels = new ArrayList<String>();
		List<String> governorWords = new ArrayList<String>();
		List<Integer> governorPositions=new ArrayList<Integer>();
		List<String> governorPosTags= new ArrayList<String>();
		List<String> dependentWords = new ArrayList<String>();
		List<Integer> dependentPositions = new ArrayList<Integer>();
		List<String> dependentPosTags = new ArrayList<String>();
		
		APSPGraph.parseASMGraphString(diseaseASMGraphString, edgeLabels 
				, governorWords,  governorPositions, governorPosTags
				, dependentWords, dependentPositions,dependentPosTags
				);
		for (int i=0; i < governorPositions.size(); i++){
			governorPositions.set(i, governorPositions.get(i) + noOfTokensInChemicalSentence);
			dependentPositions.set(i, dependentPositions.get(i) + noOfTokensInChemicalSentence);
		}
		
		String combinedASMGraphString= 
										chemicalASMGraphString
									+	APSPGraph.formASMGraphString(edgeLabels, governorWords, governorPositions, governorPosTags, dependentWords, dependentPositions, dependentPosTags)
									+   "sentenceEDGE(ROOT-0/ROOT, ROOT-"+ noOfTokensInChemicalSentence+"/ROOT)"; //this last edge, labelled sentenceEDGE is linking the roots of the two individual asm graphs
		fhNonSentenceExamples.print("|BS:asmGraph_with_entities|" + combinedASMGraphString  +"|ES|");
		
		//as linearized flat features
		DirectedGraph<Vertex,Edge> graph = APSPGraph.createASMGraphWithLinearSubgraph(combinedASMGraphString);
		Boolean use_undirected_paths = true;
		ArrayList<Path> lst_paths=  (new APSPGraph("Entity1", "Entity2")).get_all_pair_shortest_path(graph, use_undirected_paths);
		fhNonSentenceExamples.print("|BV:asmflatfeatures|"); 
		HashMap<String, Float> asm_flat_features = Path.get_asm_flatfeatures_from_allpaths(lst_paths);
		for ( String fname : asm_flat_features.keySet() ){
			fhNonSentenceExamples.print( fname.replace(":", "_") + ":" +  asm_flat_features.get(fname) + " ");
		}
		fhNonSentenceExamples.print("|EV|");
		

		fhNonSentenceExamples.println("");//end of line for end of example
	}
	

	public static void formSentenceLevelExamples(String label, Entity ec, Entity ed, 
			CoreMap sentence, PrintWriter fhSentenceExamples) throws InvalidActivityException{
		ArrayList<CoreLabel> tokens = new ArrayList<CoreLabel>( sentence.get(TokensAnnotation.class) );
		ArrayList<String> lemmas = new ArrayList<String>();
		ArrayList<Integer> token_end_boundaries = new ArrayList<Integer>();
		ArrayList<String> posTags = new ArrayList<String>();
		for( CoreLabel token : tokens){
			lemmas.add( token.get(CoreAnnotations.LemmaAnnotation.class));
			posTags.add(token.get(CoreAnnotations.PartOfSpeechAnnotation.class) );
			token_end_boundaries.add(   token.get(CoreAnnotations.CharacterOffsetEndAnnotation.class));
		}
		
		fhSentenceExamples.print(label);
		fhSentenceExamples.print("|BS:pmid|" 				+ ec.pmid + "|ES|" );
		fhSentenceExamples.print("|BS:sentenceId|" 			+ ec.sentenceId+ "|ES|" );
		fhSentenceExamples.print("|BS:sentence|" 			+ sentence.toString() + "|ES|" );
		fhSentenceExamples.print("|BS:entity1|" 		    + ec.to_string() + "|ES|" );
		fhSentenceExamples.print("|BS:entity2|" 		    + ed.to_string() + "|ES|" );
		fhSentenceExamples.print("|BS:lemmas|" 				+ String.join(" " , lemmas) + "|ES|" );
	//	fhSentenceExamples.print("|BS:sentenceBoundary|" 	+ sentence_start + "|ES|" );

		//parse structure - constituency parse
		fhSentenceExamples.print("|BT:cpTree|"				+ sentence.get(TreeAnnotation.class).toString() + "|ET|" );  

		//parse structures - basic dependency tree
		SemanticGraph basicDependencies= sentence.get(BasicDependenciesAnnotation.class);
		String dp_tree_in_lct_format =  get_dp_edges_in_parenthesis(basicDependencies, lemmas/*tokens*/, 
				posTags,  token_end_boundaries
				,ec.getStartOffsetInDocument(), ec.getEndOffsetInDocument()//ec.getStartOffsetInSentence(), ec.getEndOffsetInSentence(), 
				,ed.getStartOffsetInDocument(), ed.getEndOffsetInDocument() //ed.getStartOffsetInSentence(), ed.getEndOffsetInSentence(),
				, posTags);

		fhSentenceExamples.print("|BT:lct|" 					+ dp_tree_in_lct_format 	+ "|ET|" );
		
//		fhSentenceExamples.print("|BS:dpTree|" 				+ basicDependencies.toList().replaceAll("\n", " ")	+ "|ES|" );		
//		SemanticGraph enhancedPlusDependencies= sentence.get(EnhancedPlusPlusDependenciesAnnotation.class);		
//		fhSentenceExamples.print("|BS:dpPlusPlusGraph|"		+ enhancedPlusDependencies.toList().replaceAll("\n", " ") + "|ES|" );

		//parse structure - enhanced dependency graph
		SemanticGraph enhancedDependencies= sentence.get(EnhancedDependenciesAnnotation.class);
//		fhSentenceExamples.print("|BS:dpPlusGraph|" 			+ enhancedDependencies.toList().replaceAll("\n", " ")+ "|ES|" );
		boolean include_entities= true;
		String asm_graph_format = get_asm_format(enhancedDependencies, lemmas/*tokens*/, posTags, token_end_boundaries 
				,ec.getStartOffsetInDocument(), ec.getEndOffsetInDocument()//ec.getStartOffsetInSentence(), ec.getEndOffsetInSentence(), 
				,ed.getStartOffsetInDocument(), ed.getEndOffsetInDocument() //ed.getStartOffsetInSentence(), ed.getEndOffsetInSentence(),
				,posTags, include_entities);

		if ( !asm_graph_format.contains("Entity1")  || !asm_graph_format.contains("Entity2") ){
			throw new RuntimeException("Missing entity information for the sentence:"+ sentence.toString());
		}
		fhSentenceExamples.print("|BS:asmGraph_with_entities|" + asm_graph_format +"|ES|");
		
		//as linearized flat features
		DirectedGraph<Vertex,Edge> graph = APSPGraph.createASMGraphWithLinearSubgraph( asm_graph_format);
		Boolean use_undirected_paths = true;
		ArrayList<Path> lst_paths=  (new APSPGraph("Entity1", "Entity2")).get_all_pair_shortest_path(graph, use_undirected_paths);
		fhSentenceExamples.print("|BV:asmflatfeatures|"); 
		HashMap<String, Float> asm_flat_features = Path.get_asm_flatfeatures_from_allpaths(lst_paths);
		for ( String fname : asm_flat_features.keySet() ){
			fhSentenceExamples.print( fname.replace(":", "_") + ":" +  asm_flat_features.get(fname) + " ");
		}
		fhSentenceExamples.print("|EV|");
		


		fhSentenceExamples.println("");//end of line for end of example
	}

	
	private static String get_dp_edges_in_parenthesis(SemanticGraph dpEdges, 
			ArrayList<String> tokens, ArrayList<String> posTags, ArrayList<Integer> tokBoundaries,   
			Integer e1start, Integer e1end, Integer e2start, Integer e2end, ArrayList<String> posTags2
			){
		String graph_in_asm_format = get_asm_format(dpEdges, tokens, posTags,  tokBoundaries, e1start, e1end, e2start, e2end, posTags, false);
		//now get a directed graph from the above asm format string
		DirectedGraph<Vertex,Edge> dpGraph =  APSPGraph.createASMGraph(graph_in_asm_format);
		
		//find root first .. also ensure there is no cycle
		ArrayList<Vertex> rootNodes = new ArrayList<Vertex>();
		for ( Vertex v : dpGraph.getVertices()){
			if ( 
					(dpGraph.getPredecessors(v).isEmpty())
					&& (! v.getToken().startsWith("Entity"))   
				){
				rootNodes.add(v);
			}
		}
		
		if ( rootNodes.size() != 1){
			System.err.println("problem in get_dp_edges_in_parenthesis. finding root node . " + graph_in_asm_format);
			return "";
		}
	
		return get_lct_string(dpGraph, rootNodes.get(0), "ROOT");	
	}
	
	private static String get_lct_string(DirectedGraph<Vertex,Edge> dpGraph , Vertex node, String incomingEdgeLabel ){
		
		String retval = "(LEX##" + node.getLemma()+"::"+ node.getGeneralizedPOS() + " ";
		for (Vertex child:  dpGraph.getSuccessors(node)){
			ArrayList<Edge> edgesToChild =  new ArrayList<Edge>(dpGraph.getInEdges(child));
			if ( edgesToChild.size() != 1){
				System.err.println(" Multiple edges. Check if this is tree " 
									+ child.getCompareForm() + " : "+ dpGraph.toString() 
									);
				return "";
			}
			retval +=  get_lct_string( dpGraph, child, edgesToChild.get(0).getLabel() ) ;
		}
		
		//edge as the right most sibling
		retval +=  "(SYNT##"+ incomingEdgeLabel + "))";
		return retval;
	}
	
	private static String sanitizeTokenForASM(String input){
		Matcher matcher = Pattern.compile( "[^a-zA-Z]").matcher(input);
		return matcher.replaceAll("_na_"); //replace all non-alphabets with _na_
	}

	private static String sanitizePosForASM(String input){
		if ( input.contains("RB") || input.contains("LB") ) {
			return input;
		}

		//otherwise non-alpha must be PUNCT (dots and commas)
		Matcher matcher = Pattern.compile( "[^a-zA-Z]" ).matcher(input);
		return matcher.replaceAll("_punct_");
	}

	
	
	
	private static String get_asm_format(SemanticGraph dpEdges, 
				ArrayList<String> tokens, ArrayList<String> posTags, ArrayList<Integer> tokBoundaries,   
				int e1start, int e1end, int e2start, int e2end, ArrayList<String> posTags2,
				boolean include_entity_nodes
				)
	{

		StringBuilder buf = new StringBuilder();
		for (IndexedWord root : dpEdges.getRoots()) {
			buf.append("root(ROOT-0/ROOT, ");
			Integer targetIndex = root.get(CoreAnnotations.IndexAnnotation.class);

			String targetPos  = sanitizePosForASM (posTags.get(targetIndex -1 ));//the indices w.r.t tokens runs from 1 to n as per CoreNLP. reconvert it into 0..n-1 scale
			String targetToken= sanitizeTokenForASM (tokens.get(targetIndex -1)); //root.toString(CoreLabel.OutputFormat.VALUE_INDEX));

			buf.append(targetToken).append("-").append(targetIndex);
			buf.append("/").append( targetPos).append("); ");
		}
		for (SemanticGraphEdge edge : dpEdges.edgeListSorted()) {
			Integer srcIndex = edge.getSource().get(CoreAnnotations.IndexAnnotation.class);
			Integer targetIndex = edge.getTarget().get(CoreAnnotations.IndexAnnotation.class);

			//the indices w.r.t tokens runs from 1 to n, but in my array of postags it is 0 to n-1
			String srcPos	    = sanitizePosForASM(posTags.get(srcIndex -1));
			String targetPos    = sanitizePosForASM(posTags.get(targetIndex -1));
		
			String relationType = sanitizePosForASM( edge.getRelation().toString()  );
			String srcToken     = sanitizeTokenForASM(tokens.get(srcIndex-1) );
			String targetToken  = sanitizeTokenForASM(tokens.get(targetIndex-1));

			buf.append(relationType).append("(");
			buf.append(srcToken).append("-").append(srcIndex).append("/").append(srcPos).append(", ");
			buf.append(targetToken).append("-").append(targetIndex).append("/").append(targetPos).append("); ");
		}

		
		if (include_entity_nodes){		//attach two more entity designating nodes. - first find entity tokens
			List<Integer> e1Tokens= new ArrayList<Integer>();
			List<Integer> e2Tokens= new ArrayList<Integer>();

			Integer tokenIndex = 1;
			Integer tokenStartOffset = 0;
			for (Integer tokenEndOffset: tokBoundaries){
				//check for overlap with e1
				if ( e1end < tokenStartOffset  || e1start > tokenEndOffset) {
					// no overlap
				}else{
					e1Tokens.add( tokenIndex);
				}

				if ( e2end < tokenStartOffset  || e2start > tokenEndOffset) {
					// no overlap
				}else{
					e2Tokens.add( tokenIndex);
				}

				tokenStartOffset = tokenEndOffset + 1; //the start for the next token is 1 + the end of the current token 
				tokenIndex ++ ;
			}
			
			
			

			Integer no_of_tokens = posTags.size();
			Integer entity1NodeId = no_of_tokens + 1; //new node id being introduced for entity 1
			Integer entity2NodeId = no_of_tokens + 2; // new node id being introduced for entity 2

			//add entity 1 edge
			for (Integer targetIndex : e1Tokens ){
				String targetToken = sanitizeTokenForASM( tokens.get( targetIndex -1) 	);
				String targetPos   = sanitizePosForASM(	  posTags.get( targetIndex -1)	);
				buf.append( "Entity1(Entity1-" + entity1NodeId + "/Entity1, " + targetToken + "-" + targetIndex + "/" +targetPos + "); "    );
			}

			//add entity 2 edge
			for (Integer targetIndex : e2Tokens ){
				String targetToken = sanitizeTokenForASM( tokens.get( targetIndex -1) );
				String targetPos   = sanitizePosForASM( posTags.get( targetIndex -1) );
				buf.append( "Entity2("
						+ targetToken + "-" + targetIndex + "/" +targetPos
						+ ", "
						+ "Entity2-" + entity2NodeId + "/Entity2" 
						+ "); "    );
			}
		}// end of if (include_entity_nodes )
		return buf.toString();
	}		
}
