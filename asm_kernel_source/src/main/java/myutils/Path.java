package myutils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import it.uniroma2.sag.kelp.data.representation.vector.DenseVector;
import it.uniroma2.sag.kelp.data.representation.vector.SparseVector;


public class Path {
	String srcLabel;
	String dstLabel;
	HashMap<String, Float> pathFeatures;
	
	public static HashMap<String, Float> get_asm_flatfeatures_from_allpaths(ArrayList<Path> all_pair_sp){
		 //turn it out into the flat list of features for computation by primal method in svm
		HashMap<String,Float> all_features  = new HashMap<String, Float>();
		
		for ( Path p: all_pair_sp){
			String path_feature_name = p.srcLabel+ "_" + p.dstLabel + "_";
			for ( String pf_name : p.pathFeatures.keySet()){
				String aggregated_feature_name = path_feature_name + "_" + pf_name;
				Float oldValue = p.pathFeatures.get(pf_name);
				Float existingValue = all_features.getOrDefault(aggregated_feature_name, 0f);
				
				
				all_features.put(aggregated_feature_name, oldValue + existingValue);
				
			}
		}//for loop over all paths
		
		return all_features;
	}	
}
