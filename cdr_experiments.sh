#!/bin/bash

# This is the main bash script for performing the CID - relation extraction experiments.
# It expects a folder "cdr_data/source_files_from_cdr" and the 3 datasets CDR_DevelopmentSet.PubTator.txt, CDR_TrainingSet.PubTator.PubTator.txt, and CDR_TestSet.PubTator.txt             
# These datasets can be requested from : http://www.biocreative.org/tasks/biocreative-v/track-3-cdr.
# The next steps for the CID relation extraction are :
# Step 1) Build asm_kernel : cd asm_kernel_source ; mvn clean compile package install ; cd ..
# Step 2) Run cdr experimentation:  bash cdr_experiments.sh 
#
# Scroll down to line number 75, for the main routine.  Lines 5 to 75 are helper routines. 
#
#############################################  
PARSE_CMD="nice -n 20 java -Xmx24g -cp $CLASSPATH:./asm_kernel_source/target/asm_kernel-1.0-SNAPSHOT-jar-with-dependencies.jar  myutils.ParsePubtator "
CLASSIFY="nice -n 20 java -Xmx24g -cp $CLASSPATH:./asm_kernel_source/target/asm_kernel-1.0-SNAPSHOT-jar-with-dependencies.jar  myutils.KelpClassifier  "

function parse_cdr_files {
    mkdir -p cdr_data/parsed_files/trainset/ ; mkdir cdr_data/parsed_files/devset/ ; mkdir -p cdr_data/parsed_files/testset/

	$PARSE_CMD     cdr_data/source_files_from_cdr/CDR_DevelopmentSet.PubTator.txt           cdr_data/parsed_files/devset/     
    $PARSE_CMD     cdr_data/source_files_from_cdr/CDR_TrainingSet.PubTator.PubTator.txt     cdr_data/parsed_files/trainset/    
	$PARSE_CMD     cdr_data/source_files_from_cdr/CDR_TestSet.PubTator.txt                  cdr_data/parsed_files/testset/           

     ## combine the train and dev set into a single unit for training purpose and testing on the test dataset 
    echo "parsing complete.. preparing train and dev files"
    cat cdr_data/parsed_files/trainset/SentenceExamples.txt  cdr_data/parsed_files/devset/SentenceExamples.txt   > cdr_data/parsed_files/train_and_devset.SentenceExamples.txt 
    cat cdr_data/parsed_files/trainset/NonSentenceExamples.txt  cdr_data/parsed_files/devset/NonSentenceExamples.txt   > cdr_data/parsed_files/train_and_devset.NonSentenceExamples.txt 
    echo  "GoldAllRelations for test"
    cat  cdr_data/parsed_files/testset/GoldSentenceRelations.txt  cdr_data/parsed_files/testset/GoldNonSentenceRelations.txt  >  cdr_data/parsed_files/testset/GoldAllRelations.txt
    echo  "GoldAllRelations for dev"
    cat  cdr_data/parsed_files/devset/GoldSentenceRelations.txt  cdr_data/parsed_files/devset/GoldNonSentenceRelations.txt  >  cdr_data/parsed_files/devset/GoldAllRelations.txt
}
###########################
function classify_test_sentence_examples {
    echo "classification starts"
	$CLASSIFY cdr_data/parsed_files/train_and_devset.SentenceExamples.txt   	cdr_data/parsed_files/testset/SentenceExamples.txt	ptk-lct		 		0  CID NOT_RELATED 	
	$CLASSIFY cdr_data/parsed_files/train_and_devset.SentenceExamples.txt   	cdr_data/parsed_files/testset/SentenceExamples.txt	sstk-cpTree	 		0  CID NOT_RELATED	
	$CLASSIFY cdr_data/parsed_files/train_and_devset.SentenceExamples.txt   	cdr_data/parsed_files/testset/SentenceExamples.txt	apg-asmGraph_with_entities 	0 CID NOT_RELATED	
	$CLASSIFY cdr_data/parsed_files/train_and_devset.SentenceExamples.txt   	cdr_data/parsed_files/testset/SentenceExamples.txt	linear-asmflatfeatures 	    0 CID NOT_RELATED 
    wait
}
##############
function classify_test_non_sentence_examples {
    echo "classification starts"
    trainset="cdr_data/parsed_files/train_and_devset.NonSentenceExamples.txt"  

	$CLASSIFY $trainset   	cdr_data/parsed_files/testset/NonSentenceExamples.txt	ptk-lct		 		0	CID NOT_RELATED   
	$CLASSIFY $trainset   	cdr_data/parsed_files/testset/NonSentenceExamples.txt	sstk-cpTree	 		0	CID NOT_RELATED  
    $CLASSIFY $trainset   	cdr_data/parsed_files/testset/NonSentenceExamples.txt	linear-asmflatfeatures   	    0   CID NOT_RELATED 
	$CLASSIFY $trainset   	cdr_data/parsed_files/testset/NonSentenceExamples.txt	apg-asmGraph_with_entities	0   CID NOT_RELATED 
}
###########################
function measure_performance_test_data {
	echo  -e  "############################################################# \nperformance on the test set\n#############################################################"
	for kernelAndKelpFieldName in "ptk-lct" "sstk-cpTree" "apg-asmGraph_with_entities" "linear-asmflatfeatures" ; do 
	   echo -e "\n#######           Kernel == ", $kernelAndKelpFieldName , 
	
	   echo   "Sentence level:"
	   python measure_cdr_performance.py  cdr_data/parsed_files/testset/GoldSentenceRelations.txt  cdr_data/parsed_files/testset/SentenceExamples.txt.kelp.predictions.$kernelAndKelpFieldName 
	   echo   "Non sentence level "
	   python measure_cdr_performance.py  cdr_data/parsed_files/testset/GoldNonSentenceRelations.txt cdr_data/parsed_files/testset/NonSentenceExamples.txt.kelp.predictions.$kernelAndKelpFieldName 
	   echo  "All relations " 
	   cat cdr_data/parsed_files/testset/SentenceExamples.txt.kelp.predictions.$kernelAndKelpFieldName \
		cdr_data/parsed_files/testset/NonSentenceExamples.txt.kelp.predictions.$kernelAndKelpFieldName  \
		> cdr_data/parsed_files/testset/AllExamples.txt.kelp.predictions.$kernelAndKelpFieldName 
	   python measure_cdr_performance.py  cdr_data/parsed_files/testset/GoldAllRelations.txt cdr_data/parsed_files/testset/AllExamples.txt.kelp.predictions.$kernelAndKelpFieldName 
	
	done
	echo  "#############################################################"
}

########################################################################################
#                         main routine
############################################################################################
##step 1:  Stanford Corenlp based parsing, to get dependency parses and ASM graphs from the Pubtator files
 parse_cdr_files 

##step 2 : main experiment - classification .. testing over the test dataset, with the combined train + dev set as training for sentences and only train data for non sentences
 classify_test_sentence_examples
 classify_test_non_sentence_examples


###step 3:  measure the performance on test set - main CDR results
  measure_performance_test_data



