from __future__ import print_function

import sys

import re
import traceback 
import pdb



############# class to represent Chemical and Disease entities in the CTD pubtator files. 
class clsEntity:
    def __init__ (self, entityId, meshId, entityDescription, entityType, start, end, sentenceId, documentId):
        self.entityId = entityId
        self.meshId = meshId
        self.entityDescription = entityDescription
        self.entityType = entityType
        self.start  = start
        self.end = end
        self.documentId = documentId
        self.sentenceId = sentenceId

    def get_display(self):
        return "@".join( str(i) for i in  [self.entityId , self.meshId, self.entityDescription, self.entityType, \
                            self.start, self.end, self.sentenceId, self.documentId    ])
    ########################
    @staticmethod 
    def createEntityFromString(line):
        try :
                entityId , meshId, entityDescription, entityType, start, end, sentenceId, documentId  = line.split("@")
        except:
            print ( "prob in createEntityFromString ", line, line.split("@"),  file=sys.stderr)
            traceback.print_exc()
            sys.exit(-1)
        return clsEntity( entityId , meshId, entityDescription, entityType, int(start), int(end), int(sentenceId), documentId  )

    ################

#####################
def  compute_metrics_from_sets( actual_relations, predicted_relations):
    a , p , c  = len(actual_relations), len(predicted_relations), len(actual_relations.intersection(predicted_relations) )
    print("\t actual, predicted, correct =", a, p, c, end=" ")
    p, r = c*100.0/p , c*100.0/a 
    f = 2.0*p*r/(p+r)
    #print("\t precision, recall, f1=", p, r, f)
    return (p,r,f)

###########
def compute_metrics( gold_relations_file, predictions_file ):
    #first get the actual relations from CDR file
    actual_relations=set([])
    for line in open(gold_relations_file).readlines():
            try:
                pmid, chemid, disid =line.strip().split(":")
            except:
                print("check line", line)
                sys.exit(-1)
            actual_relations.add( (pmid,chemid,disid) )
    ##
    predicted_relations=set([])
    for line in open(predictions_file).readlines():
        label = re.match("(.*?)\|B", line).group(1).strip() #the prefix ofthe line before the first "|B" 
        if label.startswith("CID"):
            s1  = re.search( "\|BS:entity1\|(.*?)\|ES\|", line).group(1).strip()  #227508-D003000-181@D003000@clonidine@Chemical@181@190@227508 
            s2  = re.search( "\|BS:entity2\|(.*?)\|ES\|", line).group(1).strip()
            e1  = clsEntity.createEntityFromString(s1)
            e2  = clsEntity.createEntityFromString(s2)
            predicted_relations.add( (e1.documentId, e1.meshId, e2.meshId) )
    ##
    a , p , c  = len(actual_relations), len(predicted_relations), len(actual_relations.intersection(predicted_relations) )
    print("\t actual, predicted, correct =", a, p, c, end=" ")
    #p, r = c*100.0/p , c*100.0/a 
    #f = 2.0*p*r/(p+r)
    p,r,f = compute_metrics_from_sets(actual_relations, predicted_relations)
    print("\t precision, recall, f1=", p, r, f)


##################################
if __name__ == "__main__":
        compute_metrics(sys.argv[1], sys.argv[2])


