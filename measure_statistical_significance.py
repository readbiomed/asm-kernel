from __future__ import print_function

import math
import scipy 
import scipy.stats
import sys

import re
import traceback 
import pdb

from measure_cdr_ensemble_performance import clsEntity

###########
def compute_statistical_significance( cdr_test_fname, predictions_file1, predictions_file2 ):
    #following the approach of McNemar test, as described in Section 3.1 of Salzberg, Steven L. "On comparing classifiers: Pitfalls to avoid and a recommended approach." Data mining and knowledge discovery 1.3 (1997): 317-328.
    correct_labels      = [ re.match("(.*?)\|B", line).group(1).strip()  for line in open(cdr_test_fname).readlines() ]
    predicted_labels_1= [ re.match("(.*?)\|B", line).group(1).strip()  for line in open(predictions_file1).readlines() ]
    predicted_labels_2  = [ re.match("(.*?)\|B", line).group(1).strip()  for line in open(predictions_file2).readlines() ]

    ##
    s = 0 #success for algo 1
    f = 0 #failures for algo 1
    for idx, cl in enumerate(correct_labels):
        if predicted_labels_1[idx] == predicted_labels_2[idx]  : 
            continue #skip the ties
        else: 
            if predicted_labels_1[idx] == cl : # success for alg1 and failure for alg2
                s += 1
            else:
                f += 1
    #
    print("total examples, success and failures for algo1 over algo 2", len(correct_labels), s, f) 
    print("the null hypothesis is that algorithm 1 and algorithm 2 are equally accurate") #aka two tailed prediction
    #the McNemar test statistic - mc, 
    mc =     math.pow( (abs(f-s) -1) , 2) / (s+f) 
    pvalue = scipy.stats.chisqprob(mc , 1)
    print( "the statistical significance (p-value) of rejecting the null hypothesis is " , pvalue) 
    #pvalue is the inverse of the confidence. Lesser p value => higher confidence in rejecting the null hypothesis 
    

##################################
if __name__ == "__main__":
	if len(sys.argv) < 3:
		print ("<Test File with Labels>  <Predictions From Algorithm 1> <Predictions from Algorithm2 ", file=sys.stderr)
		sys.exit(-1)
	else:
	    compute_statistical_significance(sys.argv[1], sys.argv[2], sys.argv[3])
	##


