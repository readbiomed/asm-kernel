import sys
import re
import copy

import pdb 

import xml.etree.ElementTree as ET

#################################################################################




##################################################
def get_asm_format_external_entitynodes(e1TokenSpan,e2TokenSpan, listofdpedges, tokens, postags, tokenIdx):
    asm_edges= []

    #create two extra nodes - Entity1 and Entity 2 whose token ids are positions can be just outside the current sentence
    e1idx =  len(tokens) + 1 #idx here means token position
    e2idx =  len(tokens) + 2
    ##these special nodes get connected to the actual entity tokens marked by NER, with the edge label "entity1_edge" and "entity2_edge". Give their pos tags as NN
    for tokid in e1TokenSpan :
        asm_edges.append( ( "entity1_edge", "Entity1", "NN", e1idx, tokens[tokid] , postags[tokid] , tokenIdx[tokid]) )    
    for tokid in e2TokenSpan :
        asm_edges.append( ("entity2_edge", tokens[tokid] , postags[tokid] , tokenIdx[tokid],  "Entity2", "NN", e2idx ) )    

    disconnected_token_indices =   set( tokenIdx.keys() ) # clt_1, clt_2,...

    for dependency in listofdpedges :
        deptype,srcTokenId, endTokenId,   = dependency
        srctoken =  tokens[ srcTokenId ]
        endtoken =  tokens[ endTokenId ]
        srcpostag =  postags[ srcTokenId ]
        endpostag =  postags[ endTokenId ]
        srcidx =  tokenIdx [ srcTokenId ]
        endidx =  tokenIdx [ endTokenId ]
        if srcTokenId in disconnected_token_indices :
            disconnected_token_indices.remove( srcTokenId ) 
        if endTokenId in disconnected_token_indices :  
            disconnected_token_indices.remove( endTokenId ) 

        asm_edges.append(  (deptype, srctoken, srcpostag, srcidx, endtoken, endpostag, endidx) )
    ##end for 

    ##the disconnected tokens -- i.e for tokens not touched by any dependency edges, create artificial nodes so that the information of these 0-degree vertices is still recorded.
    for tokenId in disconnected_token_indices :
        deptype  =  "self" 
        srctoken =  tokens[ tokenId ]
        endtoken =  tokens[ tokenId ]
        srcpostag =  postags[ tokenId ]
        endpostag =  postags[ tokenId ]
        srcidx   =   tokenIdx[ tokenId ] 
        endidx  =    tokenIdx [ tokenId ]

        asm_edges.append(  (deptype, srctoken, srcpostag, srcidx, endtoken, endpostag, endidx) )
    ###end for

        
    ####put it back in graph for asm format 
    asmgraph = ""
    for (deptype, srctoken, srcpostag, srcidx, endtoken, endpostag, endidx) in asm_edges :
        asmgraph +=   deptype + "(" + srctoken +"-" + str(srcidx) + "/"+ srcpostag+ ", " + endtoken + "-" + str(endidx) + "/" + endpostag +"); " 
    
    ###########
    #print "after replacement ", asmgraph
    #sys.exit(-1)
    return asmgraph    

########################################################

def intersecting_spans( entitySpan, tokenSpan ):
	## <entity charOffset="65-70,82-86" id="BioInfer.d1.s1.e2" origId="e.235.6" text="muscle actin" type="Individual_protein" />
	t1, t2 = [int(i) for i in tokenSpan.split("-")]

	for p1p2 in entitySpan.split(",") :  
		p1, p2 = [int(i) for i in p1p2.split("-") ]
	
		if  (t2 < p1)  or (t1 > p2) : pass
		else :  return True
	###
	return False
########################################################
def sanitize_token_text_for_asm (tokenText):
	try :
		#finally "/" and "," s are a problem in asm's parsing
		#srctoken = srctoken.replace(",","COMMA") endtoken = endtoken.replace(",","COMMA") srctoken = srctoken.replace("/","BACKSLASH") endtoken = endtoken.replace("/","BACKSLASH")
		tokenText = re.sub("[^a-zA-Z0-9]", "_s_", tokenText) ## _s_ to denote non alpha, non numeric character
		tokenText = re.sub("[0-9]", "_d_",  tokenText) ## _d_ to denote all digits 

	except:
		print >>sys.stderr," problem sanitizing tooken text", tokenText
		sys.exit(-1)

	return tokenText

########################################################
def sanitize_postag_for_asm(srcpostag):
	#sanitize the postags also for asm format .. dots and commas show up as is in POS tags
	#except for LRB RRB , leave them as is
	if "RB" not in srcpostag : 	srcpostag = re.sub("[^a-zA-Z]", "PUNCT",srcpostag)

	return srcpostag
########################################################

def get_sentence_parse_data(e1CharSpan, e2CharSpan, sentenceElement):
    e1TokenSpan= []
    e2TokenSpan=[]
    tokenizations = sentenceElement.findall('./sentenceanalyses/tokenizations/tokenization[@tokenizer="Charniak-Lease"]')
    if len(tokenizations) != 1 :  #
        print >>sys.stderr , "should be exactly one tokenization " 
        sys.exit(-1)

    tokenization = tokenizations[0]
    postags= {}
    tokens = {}
    token_position= {}
    #tokenOffsets = {}
    tokenIdx = {}
    for tokelement in tokenization.iter('token'):
        tokenid =    tokelement.attrib['id']
        postags[ tokenid ] =  sanitize_postag_for_asm      ( tokelement.attrib['POS']     )    
        tokens[ tokenid ] =   sanitize_token_text_for_asm ( tokelement.attrib['text']     )
        tokenIdx[ tokenid ] =   len( tokenIdx)  #token idx is token position in linear order, coz tokens will be listed in linear order..
        #token_position[ tokenid ] =   tokenid.split("_")[1] #from clt_41 get 41
        #tokenOffsets[ tokenid ] =   tokelement.attrib['charOffset'] 
        tokenSpan  =   tokelement.attrib['charOffset']
        if intersecting_spans( e1CharSpan , tokenSpan): 
            e1TokenSpan.append( tokenid ) 

        if intersecting_spans( e2CharSpan , tokenSpan): 
            e2TokenSpan.append( tokenid ) 
        
    #print sentId, rawText, postags

    #now get the dependency parse
    dpedges= []
    dplst = sentenceElement.findall('./sentenceanalyses/parses/parse[@tokenizer="Charniak-Lease"][@parser="Charniak-Lease"]')
    if len(dplst) != 1 :
        print >>sys.stderr , "should be exactly one dependency parse " 
        sys.exit(-1)

    dpinfo = dplst[0]
    for dependency in dpinfo.iter('dependency'):
        #"split_1" t1="st_4" t2="st_3" type="aux"    
        deptype =  dependency.attrib['type']
        srcid =  dependency.attrib['t1']  #is of the form clt_2, clt_3..
        endid =  dependency.attrib['t2']
        srctoken = tokens [ srcid] 
        endtoken = tokens [ endid ]
        srcpos   =  postags[ srcid ]
        endpos   =  postags [endid]
        
        dpedges.append(  (deptype, srcid, endid) )

    #print "|BS:sentenceId|",sentId,"|ES|",
    #print "|BS:sentenceDetails|", rawText, "|ES|",
    #print "|BT:dpTree|", dpedges

    asm_graph =    get_asm_format_external_entitynodes( e1TokenSpan, e2TokenSpan, dpedges, tokens, postags, tokenIdx)

    return asm_graph

############################
def process_xml_file(fname , outfname):
	tree = ET.parse(fname)
	root = tree.getroot()
	
	ofh = open(outfname, "w")

	#find all pairs and write then out one per file in kelp format
	for pair in root.findall( './document/sentence/pair' ):	
		pairid = pair.attrib['id']
		pairLabel  = pair.attrib['interaction']
		e1id = pair.attrib['e1']
		e2id = pair.attrib['e2'] #like AIMed.d0.s5.e2
		sentenceId =  e1id[ 0 :  e1id.rfind(".")  ]  #contained as part of entity id

		sentLst =  root.findall('./document/sentence/[@id="' +sentenceId+ '"]')
		if len(sentLst) != 1 : 
			print >>sys.stderr, "multiple sentences for sentence id ", sentenceId
			sys.exit(-1)
       		rawText = sentLst[0].attrib['text']
	
		#next find the entity's character offset-span
		ent1Lst = root.findall('./document/sentence/entity[@id="' +e1id+  '"]')	
		ent2Lst = root.findall('./document/sentence/entity[@id="' +e2id+  '"]')	
		if len(ent1Lst)!=1 or len(ent2Lst)!=1 :
			print >>sys.stderr, "multiple entity elements for ", e1id, e2id
			sys.exit(-1)

		e1Span = ent1Lst[0].attrib['charOffset']
		e2Span = ent2Lst[0].attrib['charOffset']
		e1Text = ent1Lst[0].attrib['text']
		e2Text = ent2Lst[0].attrib['text']

		## add if posible, sentenceId, e1start, e1end (look in charspan)
		#rename entity1Text  to entity1Details - same for entity2
		try : #if span contains "," .. ie multiple char spans.. just take the first one - helpless
			tarr = e1Span.split(",")[0].split("-")
			e1start, e1end = int(tarr[0]), int(tarr[1])
			tarr = e2Span.split(",")[0].split("-")
			e2start, e2end = int(tarr[0]), int(tarr[1])
		except:
			print >>sys.stderr, "problem in parsing ", e1Span, e2Span
			sys.exit(-1)

		print >>ofh, pairLabel,	
		print >>ofh,   "|BS:pairLabel|", pairLabel, "|ES|",		
		print >>ofh,  "|BS:pairid|", pairid , "|ES|" ,
		print >>ofh,  "|BS:sentence|", rawText , "|ES|" ,
		print >>ofh,  "|BS:sentenceId|", sentenceId , "|ES|" ,
		print >>ofh,  "|BS:entity1|", e1id , "|ES|" ,
		print >>ofh,  "|BS:entity2|", e2id , "|ES|" ,
		print >>ofh,  "|BS:entity1Details|", e1Text , "|ES|" , #print >>ofh,  "|BS:entity1Text|", e1Text , "|ES|" ,
		print >>ofh,  "|BS:entity2Details|", e2Text , "|ES|" , #print >>ofh,  "|BS:entity2Text|", e2Text , "|ES|" , 
		asmgraph  = get_sentence_parse_data( e1Span, e2Span, sentLst[0] )
		print >>ofh,  "|BS:asmgraph_plus|",  asmgraph , "|ES|" ,
		print >>ofh,  "|BS:e1start|", e1start, "|ES|",
		print >>ofh,  "|BS:e1end|"  , e1end  , "|ES|",
		print >>ofh,  "|BS:e2start|", e2start, "|ES|",
		print >>ofh,  "|BS:e2end|"  , e2end  , "|ES|",
		print >>ofh 

	########3
	ofh.close()

	print >>sys.stderr, "done with ", fname

#######################################
def compute_metrics(predictionsFile, originalXmlFile):
    predictions = {}
    lineIdx = 0
    ifh =open(predictionsFile)
    while True:
        line =  ifh.readline()
        if not line : break
        predictedLabel = re.match("(.*?)\|B", line).group(1).strip() #the prefix ofthe line before the first "|B" 
        pairid  = re.search( "\|BS:pairid\|(.*?)\|ES\|", line).group(1).strip()
        predictions[pairid] = predictedLabel
        lineIdx += 1
    #####

    goldLabels = {}
    tree = ET.parse(originalXmlFile)
    root = tree.getroot()
    for pair in root.findall( './document/sentence/pair' ):    
        pairid = pair.attrib['id']
        pairLabel  = pair.attrib['interaction']
        goldLabels[pairid] = pairLabel

    ##compute metrics
    tp = tn = fp = fn = 0
    allpairIds =  set(goldLabels.keys()).intersection( set(predictions.keys()) ) ## In the validation setting, originalXmlFile will contain far more pair ids, but we need to only worry about the pairs in the prediction file . In the full test scenario, they should be equal
    for pairid in allpairIds :
        predictedLabel = "False"
        goldLabel     = "False"    
        if pairid not in goldLabels :
            print >>sys.stderr, "recheck gold labels for pair id ", pairid
            sys.exit(-1)
        else:
            goldLabel = goldLabels [ pairid ]

        if pairid not in predictions :
            print >>sys.stderr, "missing pair in kelp ", pairid
        else:
            predictedLabel = predictions[pairid]

        ##
        if predictedLabel == "True"  and goldLabel == "True": 
            tp += 1
        elif predictedLabel == "True"  and goldLabel == "False":
            fp += 1
        elif predictedLabel == "False"  and goldLabel == "True": 
            fn += 1        
        elif predictedLabel == "False"  and goldLabel == "False": 
            tn += 1
        else:
            print >>sys.stderr, "check labels ", predictedLabel, goldLabel
            sys.exit(-1)

    #### final metrics
    try :
        p = tp*1.0/(tp+fp)
        r = tp*1.0/(tp+fn)
        f= 2.0*p*r/(p+r)
        accuracy = (tp + tn)*1.0/(tp+tn+fp+fn) 
    except:
        p,r,f, accuracy = 0, 0, 0, 0

    print "metrics for ", predictionsFile , " tp, tn , fp, fn " , tp, tn, fp, fn , "of the total pair ids ", (tp+tn+fp+fn)
    print "prec,recall,f1, accuracy", p, r, f, accuracy
    
    
                        
###############################################################################################
def  produce_train_test_splits(original_kelp_file, split_definition, split_kelpfile ) :
    #first read all the pairs and the corresponding lines form the original kelp file
    pairIdToKelpInfo = {}
    for line in open(original_kelp_file).readlines():
        pairid  = re.search("\|BS:pairid\|(.*?)\|ES\|", line).group(1).strip()
	#print pairid	
        pairIdToKelpInfo [ pairid ] =  line.strip()
    ########

    split_pairids =[]
    #now find out the pair ids defined in the split
    tree = ET.parse(split_definition)
    root = tree.getroot()
    for pair in root.findall( './document/sentence/pair' ):	
	pairid = pair.attrib['id']
        split_pairids.append ( pairid )
    ###

    #write it out in the split specific kelp file
    ofh = open(split_kelpfile, "w")
    for pairid in split_pairids :
        print >>ofh, pairIdToKelpInfo[pairid]
    ofh.close()

    print >>sys.stderr, "done"

##########################
def produce_split_wise_kelp_files():
    all_split_files= [ \
            "/home/nagesh/bionlp/datasets/protein_protein_interaction/ppi-eval-standard/train/AIMed-train.xml",\
            "/home/nagesh/bionlp/datasets/protein_protein_interaction/ppi-eval-standard/test/AIMed-test.xml" ,\

            "/home/nagesh/bionlp/datasets/protein_protein_interaction/ppi-eval-standard/train/BioInfer-train.xml"  ,\
            "/home/nagesh/bionlp/datasets/protein_protein_interaction/ppi-eval-standard/test/BioInfer-test.xml", \

            "/home/nagesh/bionlp/datasets/protein_protein_interaction/ppi-eval-standard/train/HPRD50-train.xml"  ,\
            "/home/nagesh/bionlp/datasets/protein_protein_interaction/ppi-eval-standard/test/HPRD50-test.xml",\

            "/home/nagesh/bionlp/datasets/protein_protein_interaction/ppi-eval-standard/train/IEPA-train.xml", \
            "/home/nagesh/bionlp/datasets/protein_protein_interaction/ppi-eval-standard/test/IEPA-test.xml",\

            "/home/nagesh/bionlp/datasets/protein_protein_interaction/ppi-eval-standard/train/LLL-train.xml", \
            "/home/nagesh/bionlp/datasets/protein_protein_interaction/ppi-eval-standard/test/LLL-test.xml",\
            ]

    for fname in all_split_files:
        split_xml = fname
        split_kelp= fname.replace(".xml", ".kelp")
        if "AIMed" in fname         : original_file = "AImed-learning-format.kelp"
        elif "BioInfer" in fname    : original_file = "./BioInfer-learning-format.kelp"
        elif "HPRD" in fname        : original_file = "./HPRD50-learning-format.kelp"  
        elif "IEPA" in fname        : original_file = "./IEPA-learning-format.kelp"    
        elif "LLL" in fname         : original_file = "./LLL-learning-format.kelp"   
        else: 
            print >>sys.stderr, "i give up: produce_split_wise_kelp_files"
            sys.exit(-1)

        produce_train_test_splits(original_file, split_xml, split_kelp)
        ##"./AImed-learning-format.kelp" , "../../protein_protein_interaction/ppi-eval-standard/train/AIMed-train.xml" ,  "../../protein_protein_interaction/ppi-eval-standard/train/AIMed-train.kelp" )

############################
if __name__ == "__main__" :
	if sys.argv[1] == "prepare_kelp_files" :
		#########convert_xml_to_kelp("../AImed-learning-format.xml")
		process_xml_file("./ppi_data/AImed-learning-format.xml" , 		"./ppi_data/AImed-learning-format.asmgraph")
		process_xml_file("./ppi_data/BioInfer-learning-format.xml",  	"./ppi_data/BioInfer-learning-format.asmgraph")
		process_xml_file("./ppi_data/HPRD50-learning-format.xml",    	"./ppi_data/HPRD50-learning-format.asmgraph"  )
		process_xml_file("./ppi_data/IEPA-learning-format.xml",      	"./ppi_data/IEPA-learning-format.asmgraph"    )
		process_xml_file("./ppi_data/LLL-learning-format.xml",       	"./ppi_data/LLL-learning-format.asmgraph"   )
		###
		##    produce_split_wise_kelp_files()
	elif sys.argv[1] == "compute_metrics":
		compute_metrics(sys.argv[2], sys.argv[3]) 
	else:
		print >>sys.stderr, "Check arguments "

        
