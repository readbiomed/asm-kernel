

#######   step 1 : download the PPI corpora from  mars.cs.utu.fi  -
#  AImed-learning-format.xml.gz    
#  BioInfer-learning-format.xml.gz
#  HPRD50-learning-format.xml.gz
#  IEPA-learning-format.xml.gz
#  LLL-learning-format.xml.gz

# unzip them and place them in the folder ./ppi_data/
#cd ppi_data ; gunzip * ; cd ..  
#
#########     step 2 : convert PPI corpora to kelp format 
python ppi_data_parser.py prepare_kelp_files  ## produces the asmgraph
##
GENERATE_ASM="nice -n 15 java -Xmx15g -cp $CLASSPATH:./asm_kernel_source/target/asm_kernel-1.0-SNAPSHOT-jar-with-dependencies.jar  myutils.APSPGraph   "  #produces asm flatfeatures
CLASSIFY="nice -n 15 java -Xmx15g -cp $CLASSPATH:./asm_kernel_source/target/asm_kernel-1.0-SNAPSHOT-jar-with-dependencies.jar  myutils.KelpClassifier  "

$GENERATE_ASM    ppi_data/AImed-learning-format.asmgraph          asmgraph_plus  ppi_data/AImed-learning-format.kelp         
$GENERATE_ASM    ppi_data/BioInfer-learning-format.asmgraph       asmgraph_plus  ppi_data/BioInfer-learning-format.kelp      
$GENERATE_ASM    ppi_data/HPRD50-learning-format.asmgraph         asmgraph_plus  ppi_data/HPRD50-learning-format.kelp        
$GENERATE_ASM    ppi_data/IEPA-learning-format.asmgraph           asmgraph_plus  ppi_data/IEPA-learning-format.kelp          
$GENERATE_ASM    ppi_data/LLL-learning-format.asmgraph            asmgraph_plus  ppi_data/LLL-learning-format.kelp           
                 
##########    step 3 : prepare classification files for cross corpus learning - test on each of the 5 corpora, with the remaining as the training data - random shuffle them too 
cat ppi_data/AImed-learning-format.kelp  ppi_data/BioInfer-learning-format.kelp  ppi_data/HPRD50-learning-format.kelp  ppi_data/IEPA-learning-format.kelp \
        >  ppi_data/training_data_for_LLL-learning-format.kelp
cat ppi_data/AImed-learning-format.kelp  ppi_data/BioInfer-learning-format.kelp  ppi_data/HPRD50-learning-format.kelp   ppi_data/LLL-learning-format.kelp \
        >  ppi_data/training_data_for_IEPA-learning-format.kelp
cat ppi_data/AImed-learning-format.kelp  ppi_data/BioInfer-learning-format.kelp    ppi_data/IEPA-learning-format.kelp  ppi_data/LLL-learning-format.kelp \
        >  ppi_data/training_data_for_HPRD50-learning-format.kelp
cat ppi_data/AImed-learning-format.kelp    ppi_data/HPRD50-learning-format.kelp  ppi_data/IEPA-learning-format.kelp  ppi_data/LLL-learning-format.kelp \
        >  ppi_data/training_data_for_BioInfer-learning-format.kelp
cat  ppi_data/BioInfer-learning-format.kelp  ppi_data/HPRD50-learning-format.kelp  ppi_data/IEPA-learning-format.kelp  ppi_data/LLL-learning-format.kelp \
        >  ppi_data/training_data_for_AImed-learning-format.kelp



#########  step 3 : classify with apg and asm kernels
echo "classification starts with apg kernel..."
    $CLASSIFY ppi_data/training_data_for_AImed-learning-format.kelp      ppi_data/AImed-learning-format.kelp             apg-asmgraph_plus 0  True  False  
    $CLASSIFY ppi_data/training_data_for_HPRD50-learning-format.kelp     ppi_data/HPRD50-learning-format.kelp            apg-asmgraph_plus 0  True  False  
    $CLASSIFY ppi_data/training_data_for_LLL-learning-format.kelp        ppi_data/LLL-learning-format.kelp               apg-asmgraph_plus 0  True  False  
    $CLASSIFY ppi_data/training_data_for_BioInfer-learning-format.kelp   ppi_data/BioInfer-learning-format.kelp          apg-asmgraph_plus 0  True  False  
    $CLASSIFY ppi_data/training_data_for_IEPA-learning-format.kelp       ppi_data/IEPA-learning-format.kelp              apg-asmgraph_plus 0  True  False
echo "classification starts with asm kernel..."
    $CLASSIFY ppi_data/training_data_for_AImed-learning-format.kelp      ppi_data/AImed-learning-format.kelp             linear-asmflatfeatures 0  True  False 
    $CLASSIFY ppi_data/training_data_for_HPRD50-learning-format.kelp     ppi_data/HPRD50-learning-format.kelp            linear-asmflatfeatures 0  True  False 
    $CLASSIFY ppi_data/training_data_for_LLL-learning-format.kelp        ppi_data/LLL-learning-format.kelp               linear-asmflatfeatures 0  True  False 
    $CLASSIFY ppi_data/training_data_for_BioInfer-learning-format.kelp   ppi_data/BioInfer-learning-format.kelp          linear-asmflatfeatures 0  True  False 
    $CLASSIFY ppi_data/training_data_for_IEPA-learning-format.kelp       ppi_data/IEPA-learning-format.kelp              linear-asmflatfeatures 0  True  False 
echo "classification done"



############## step 4 : measure performance
## see  table 3 in https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2895635/
## as per that paper, we expect f-scores of 
#Expected:               43.8    39.1    69.7    59.6    72.3 for Aimed, BioInfer, HPRD, IEPA and LLL respectively 
#Observed with APG       41      44      65      60      71
#Observed with ASM       36      39      65      58      53

echo -e  "\n        AImed performance numbers for apg and asm. Excepted fscore 43" 
python ppi_data_parser.py  compute_metrics ppi_data/AImed-learning-format.kelp.kelp.predictions.apg-asmgraph_plus     ppi_data/AImed-learning-format.xml  
python ppi_data_parser.py  compute_metrics ppi_data/AImed-learning-format.kelp.kelp.predictions.linear-asmflatfeatures  ppi_data/AImed-learning-format.xml  

echo -e  "\n        BioInfer performance numbers for apg and asm.. Expected fscore 39 " 
python ppi_data_parser.py  compute_metrics ppi_data/BioInfer-learning-format.kelp.kelp.predictions.apg-asmgraph_plus     ppi_data/BioInfer-learning-format.xml  
python ppi_data_parser.py  compute_metrics ppi_data/BioInfer-learning-format.kelp.kelp.predictions.linear-asmflatfeatures  ppi_data/BioInfer-learning-format.xml  

echo -e  "\n        HPRD50 performance numbers for apg and asm. Expected fscore 69 " 
python ppi_data_parser.py  compute_metrics ppi_data/HPRD50-learning-format.kelp.kelp.predictions.apg-asmgraph_plus      ppi_data/HPRD50-learning-format.xml  
python ppi_data_parser.py  compute_metrics ppi_data/HPRD50-learning-format.kelp.kelp.predictions.linear-asmflatfeatures   ppi_data/HPRD50-learning-format.xml  

echo -e  "\n        IEPA performance numbers for apg and asm. Expected fscore 59 " 
python ppi_data_parser.py  compute_metrics ppi_data/IEPA-learning-format.kelp.kelp.predictions.apg-asmgraph_plus      ppi_data/IEPA-learning-format.xml  
python ppi_data_parser.py  compute_metrics ppi_data/IEPA-learning-format.kelp.kelp.predictions.linear-asmflatfeatures   ppi_data/IEPA-learning-format.xml  

echo -e  "\n        LLL performance numbers for apg and asm. Expected fscore 72 " 
python ppi_data_parser.py  compute_metrics ppi_data/LLL-learning-format.kelp.kelp.predictions.apg-asmgraph_plus       ppi_data/LLL-learning-format.xml  
python ppi_data_parser.py  compute_metrics ppi_data/LLL-learning-format.kelp.kelp.predictions.linear-asmflatfeatures    ppi_data/LLL-learning-format.xml  


